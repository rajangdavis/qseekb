class ServiceProductController < ApplicationController
  def show
  end

  def index
  end

  def test

  	roots_arr = []

  	roots = ServiceProduct.where('parent_rn_id IS NULL')

  	roots.each { |sc| ServiceProduct.where('parent_rn_id = ?',sc.id).each {|series| roots_arr.push(series.name)}}

  	@service_products = roots_arr

  end
end
