class JsonResponseController < ApplicationController
	def getAllProducts

		@products = JsonResponse.getAllProducts

		respond_to do |format|
			format.json { render :json => @products }
		end
	end

	def getAllAnswers

		@answers = JsonResponse.getAllAnswers

		respond_to do |format|
			format.json { render :json => @answers }
		end
	end

end
