class Answer < ActiveRecord::Base

	def to_hash_format
		# added .to_s because I didn't catch 
		# that I messed up and forgot to turn
		# integers into integers on OSC
		
		@temp_prod_hash = {}

		@temp_prod_hash[:answer_id] = self.id.to_s

		@temp_prod_hash[:Summary] = self.title

		@temp_prod_hash[:Category] = self.get_main_category

		@temp_prod_hash[:cat_lvl_2] = self.grand_parent_category_ids

		# @temp_prod_hash[:Category] = self.get_main_category

		@temp_prod_hash[:prod_lvl_3] = @temp_prod_hash[:Category] == 'Firmware' ? self.title.split(' :')[0] : ""

		@temp_prod_hash[:prod_lvl_2] = @temp_prod_hash[:Category] == 'Firmware' ? ServiceProduct.find_by(name: @temp_prod_hash[:Summary].split(' :')[0]) == nil ? "" : ServiceProduct.find_by(name: @temp_prod_hash[:Summary].split(' :')[0]).parent_name : self.grand_parent_product_ids[:parent]
		
		@temp_prod_hash[:prod_lvl_1] = @temp_prod_hash[:Category] == 'Firmware' ? ServiceProduct.find_by(name: @temp_prod_hash[:prod_lvl_2]) == nil ? "" : ServiceProduct.find_by(name: @temp_prod_hash[:prod_lvl_2]).parent_name : self.grand_parent_product_ids[:grand_parent]
		
		@temp_prod_hash
	end

	def get_main_category

		main_category = ''

		self.rn_service_category_ids.each do |id|
			@service_category = ServiceCategory.where('parent_id IS NULL AND id = ?', id)
			if @service_category.first
				main_category = @service_category.first.name
			end
		end

		main_category
	end


	def grand_parent_product_ids
		@products = {}
		@products[:parent] = ""
		@products[:grand_parent] = ""

		if self.rn_service_product_ids.last!=nil
			@sp = ServiceProduct.find_by(id: self.rn_service_product_ids.last)
			if @sp!= nil
				@sp_parent = @sp.parent_id
				@sp_parent_obj = ServiceProduct.find_by(id: @sp_parent)
				if @sp_parent_obj!=nil
					@products[:parent] = @sp_parent_obj.name
					@sp_grand_parent_object = ServiceProduct.find_by(id: @sp_parent_obj.parent_id)
					if @sp_grand_parent_object!=nil
						@products[:grand_parent] = @sp_grand_parent_object.name
					else
						@products[:grand_parent] = ""
					end
				end
			end
		end
		@products
	end


	def grand_parent_category_ids
		@categories = {}
		@categories[:child] = ""
		@categories[:grand_child] = ""

		if self.rn_service_category_ids!=nil
			@first_category_id = self.rn_service_category_ids[0]
			self.rn_service_category_ids.each do |id|
				@cat_lvl_2 = ServiceCategory.find_by(id: id)
				if @cat_lvl_2!=nil  && @cat_lvl_2.parent_id==@first_category_id
					@categories[:child] = @cat_lvl_2.name
				end
			end
		else
			@categories[:child] = nil
			@categories[:grand_child] = nil	
		end
		@categories[:child]
	end

end