class JsonResponse

	def self.getAllProducts

		@products = ServiceProduct.all

		@products_final = []

		@products_object_final = {}

		@products.each do |p|

			if !p.grand_parent_name.blank?
				@products_final.push({p.grand_parent_name => p.simple_obj})
			end
		end

		@products_final = @products_final.flat_map(&:entries).group_by(&:first).map{|k,v| {k => v.map(&:last)}}

		@products_final.each_with_index do |k,v|
			k.each do|key,val|
				@products_object_final[key] = val
			end
		end

		@products_object_final.to_json
	end

	def self.getAllAnswers
		
		@answers = Answer.where('status_id = 4')

		@answers_list = make_tree(@answers)

		@answers_list

	end

	def self.categories_hash

		@categories_hash_intermediate = {}

		@categories_hash_final = {}

		@categories_hash = ServiceCategory.find_by_sql(
			'WITH RECURSIVE category_tree(id, name, path) AS (
			  SELECT id, name, ARRAY[id]
			  FROM service_categories
			  WHERE parent_id IS NULL
			UNION ALL
			  SELECT service_categories.id, service_categories.name, path || service_categories.id
			  FROM category_tree
			  JOIN service_categories ON service_categories.parent_id=category_tree.id
			  WHERE NOT service_categories.id = ANY(path)
			)
			SELECT * FROM category_tree ORDER BY path;'
		)

		@categories_hash.each do |ch|

			@ch_id = ch.id

			@categories_hash_intermediate[@ch_id] = ch

		end

		@categories_hash.each do |ch|

			ch_path_final = []

			ch[:path].each do |c|
				ch_path_final.push(@categories_hash_intermediate[c][:name])
			end

			@categories_hash_final[ch[:id]] =  ch_path_final
		end

		@categories_hash_final
	end

	def self.products_hash

		@products_hash_intermediate = {}

		@products_hash_final = {}

		@products_hash = ServiceProduct.find_by_sql(
			'WITH RECURSIVE product_tree(id, name, path) AS (
			  SELECT id, name, ARRAY[id]
			  FROM service_products
			  WHERE parent_id IS NULL
			UNION ALL
			  SELECT service_products.id, service_products.name, path || service_products.id
			  FROM product_tree
			  JOIN service_products ON service_products.parent_id=product_tree.id
			  WHERE NOT service_products.id = ANY(path)
			)
			SELECT * FROM product_tree ORDER BY path;'
		)

		@products_hash.each do |ph|

			@ph_id = ph.id

			@products_hash_intermediate[@ph_id] = ph

		end

		@products_hash.each do |ph|

			ph_path_final = []

			ph[:path].each do |p|
				ph_path_final.push(@products_hash_intermediate[p][:name])
			end

			@products_hash_final[ph[:id]] =  ph_path_final
		end

		@products_hash_final
	end

	def self.make_tree(objects)

		@answer_array = []
		@answer_array_first_pass = []
		@categories = categories_hash
		@products = products_hash				
		
		
		objects.each do |object|

			object.rn_service_category_ids.each do |rn_sc_id|
				@answer_array_first_pass.push({:id => object.id, :Summary => object.title,:Category => @categories[rn_sc_id].first, 
									:cat_lvl_2 => @categories[rn_sc_id][1], :cat_lvl_3 => @categories[rn_sc_id][2], :prods => object.rn_service_product_ids})
			end

		end

           
    #         if($row['cat_lvl_2']!=null && $row['prod_lvl_1']!=null && $row['prod_lvl_2']!=null){
    #             if($row['Category'] == 'User Documentation'){
    #                 if($row['cat_lvl_3']!=null){
    #                     $data[] = $row;
    #                 }
    #             }else{
    #                 $data[] = $row;
    #             }
    #         }

		@answer_array_first_pass.each do |ans|
			ans[:prods].each do |prod|
				@ans_id = ans[:id].to_s					
				if ans[:Category]=='Firmware' && @products[prod]!=nil && @products[prod][0]!=nil && @products[prod][1]!=nil
					@prod_lvl_3 = ans[:Summary].split(":")[0].squish!

					@answer_object = {:answer_id => @ans_id, :Summary => ans[:Summary],:Category => ans[:Category], 
									:cat_lvl_2 => nil, :cat_lvl_3 => nil, 
									:prod_lvl_1 => @products[prod][0], :prod_lvl_2 => @products[prod][1], :prod_lvl_3 => @prod_lvl_3}

					if !@answer_array.include? @answer_object

						@answer_array.push(@answer_object)

					end
				elsif ans[:cat_lvl_2]!=nil && @products[prod]!=nil && @products[prod][0]!=nil && @products[prod][1]!=nil
					if ans[:Category] == 'How-tos'
						if ans[:cat_lvl_3]!=nil && @products[prod].count == 2
							@answer_object = {:answer_id => @ans_id, :Summary => ans[:Summary],:Category => ans[:Category], 
										:cat_lvl_2 => ans[:cat_lvl_2], :cat_lvl_3 => ans[:cat_lvl_3], 
										:prod_lvl_1 => @products[prod][0], :prod_lvl_2 => @products[prod][1]}
							if !@answer_array.include? @answer_object

								@answer_array.push(@answer_object)

							end
						end
					elsif @products[prod]!=nil && @products[prod].count == 2
						@answer_object = {:answer_id => @ans_id, :Summary => ans[:Summary],:Category => ans[:Category], 
										:cat_lvl_2 => ans[:cat_lvl_2], :cat_lvl_3 => ans[:cat_lvl_3], 
										:prod_lvl_1 => @products[prod][0], :prod_lvl_2 => @products[prod][1]}
						if !@answer_array.include? @answer_object

							@answer_array.push(@answer_object)

						end
					end
				end
			end
		end
		
		@answer_array.to_json.gsub!(/null/,'""')
	end

end
