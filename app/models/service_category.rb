class ServiceCategory < ActiveRecord::Base

	belongs_to :parent, class_name: "ServiceCategory"
  	has_many :children, class_name: "ServiceCategory", foreign_key: "parent_id"

	def descendents
		self_and_descendents - [self]
	end

	def self_and_descendents
		self.class.tree_for(self)
	end

	def descendent_categories
		subtree = self.class.tree_sql_for(self)
		ServiceCategory.where("category_id IN (#{subtree})")
	end

	def self.tree_for(instance)
		where("#{table_name}.id IN (#{tree_sql_for(instance)})").order("#{table_name}.id")
	end

	def self.tree_sql_for(instance)
		tree_sql =  <<-SQL
		  WITH RECURSIVE search_tree(id, path) AS (
		      SELECT id, ARRAY[id]
		      FROM #{table_name}
		      WHERE id = #{instance.id}
		    UNION ALL
		      SELECT #{table_name}.id, path || #{table_name}.id
		      FROM search_tree
		      JOIN #{table_name} ON #{table_name}.parent_id = search_tree.id
		      WHERE NOT #{table_name}.id = ANY(path)
		  )
		  SELECT id FROM search_tree ORDER BY path
		SQL
	end

	def parent_name
	  # it may not have a parent
	  parent.try(:name)
	end

	def grand_parent_name
	  if parent.present?
	  	parent.parent.try(:name)
	  end
	end
	
	def children_ids
		children_ids = []
		if self.children.count > 0
			children_ids = self.children.map { |x| x[:id] }
		end
		children_ids
	end

end
