class ServiceProduct < ActiveRecord::Base

	belongs_to :parent, class_name: "ServiceProduct"
  	has_many :children, class_name: "ServiceProduct", foreign_key: "parent_id"


  	def descendents
		self_and_descendents - [self]
	end

	def self_and_descendents
		self.class.tree_for(self)
	end

	def descendent_products
		subtree = self.class.tree_sql_for(self)
		ServiceProduct.where("product_id IN (#{subtree})")
	end

	def self.tree_for(instance)
		where("#{table_name}.id IN (#{tree_sql_for(instance)})").order("#{table_name}.id")
	end

	def self.tree_sql_for(instance)
		tree_sql =  <<-SQL
		  WITH RECURSIVE search_tree(id, path) AS (
		      SELECT id, ARRAY[id]
		      FROM #{table_name}
		      WHERE id = #{instance.id}
		    UNION ALL
		      SELECT #{table_name}.id, path || #{table_name}.id
		      FROM search_tree
		      JOIN #{table_name} ON #{table_name}.parent_id = search_tree.id
		      WHERE NOT #{table_name}.id = ANY(path)
		  )
		  SELECT id FROM search_tree ORDER BY path
		SQL
	end

	def simple_obj
		@product_object = Hash.new
		@product_object[:id] = self.id
		@product_object[:name] = self.name
		@product_object[:series] = self.parent_name
		@product_object[:technology] = self.technology
		@product_object[:product_type] = self.grand_parent_name
		# @product_object[:supports] = self.supports
		# @product_object[:nested_children_for_rmas] = self.nested_children_for_rmas
		@product_object
	end
	

	def parent_name
	  # it may not have a parent
	  parent.try(:name)
	end

	def grand_parent_name
	  if parent.present?
	  	parent.parent.try(:name)
	  end
	end

	# def parent_name
	# 	parent = nil
	# 	if self.parent_id!= nil
	# 		@sp_obj = ServiceProduct.find_by(id: self.parent_id)
	# 		if @sp_obj!=nil
	# 			parent = @sp_obj.name
	# 		end
	# 	end
	# 	parent
	# end

	# def grand_parent_name
	# 	grand_parent_name = nil
	# 	if self.parent_name!= nil and self.parent_id!= nil
	# 		@sp_obj = ServiceProduct.find_by(id: self.parent_id)
	# 		if @sp_obj!=nil
	# 			@sp_grandpa_obj = ServiceProduct.find_by(id: @sp_obj.parent_id)
	# 			if @sp_grandpa_obj
	# 				grand_parent_name = @sp_grandpa_obj.name
	# 			end
	# 		end
	# 	end
	# 	grand_parent_name
	# end

end