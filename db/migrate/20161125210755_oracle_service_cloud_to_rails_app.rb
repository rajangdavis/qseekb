class OracleServiceCloudToRailsApp < ActiveRecord::Migration
  def change
  	
    create_table :service_categories do |t|
      t.string :name 
  	  # t.integer :rn_id
  	  t.integer :parent_id
      t.timestamps null: false
    end

    create_table :service_products do |t|
      # t.integer :rn_id
      t.integer :parent_id
      t.string :name 
      t.string :camera_type 
      t.string :technology 
      t.string :supports, array: true
      t.string :nested_children_for_rmas, array: true
      t.timestamps null: false
    end
  	
  	create_table :answers do |t|

      t.timestamps null: false
      t.string :name 
  	  t.integer :status_id
	    # t.integer :rn_id
      t.integer :file_attachment_id
	    t.string :title
  	  t.string :title_jp
	    t.string :title_fr
	    t.string :tagline
	    t.string :tagline_jp
	    t.string :tagline_fr
	    t.string :keywords
	    t.integer :rn_service_product_ids, default: [], array: true
	    t.integer :rn_service_category_ids, default: [], array: true
	    t.integer :rn_language_ids, default: [], array: true
    end

    create_table :answer_types do |t|
      # t.integer :rn_id
      t.string :label
    end

    create_table :answer_statuses do |t|
      # t.integer :rn_id
      t.string :label
    end

    create_table :site_languages do |t|
      # t.integer :rn_id
      t.string :label
    end

  end

end
