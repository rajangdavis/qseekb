# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161125210755) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answer_statuses", force: :cascade do |t|
    t.string "label"
  end

  create_table "answer_types", force: :cascade do |t|
    t.string "label"
  end

  create_table "answers", force: :cascade do |t|
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "name"
    t.integer  "status_id"
    t.integer  "file_attachment_id"
    t.string   "title"
    t.string   "title_jp"
    t.string   "title_fr"
    t.string   "tagline"
    t.string   "tagline_jp"
    t.string   "tagline_fr"
    t.string   "keywords"
    t.integer  "rn_service_product_ids",  default: [],              array: true
    t.integer  "rn_service_category_ids", default: [],              array: true
    t.integer  "rn_language_ids",         default: [],              array: true
  end

  create_table "service_categories", force: :cascade do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_products", force: :cascade do |t|
    t.integer  "parent_id"
    t.string   "name"
    t.string   "camera_type"
    t.string   "technology"
    t.string   "supports",                              array: true
    t.string   "nested_children_for_rmas",              array: true
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "site_languages", force: :cascade do |t|
    t.string "label"
  end

end
