var FIREBASE_URL = "https://qseeupdates.firebaseio.com/";
var SUPPORT_NEWS = new Firebase(FIREBASE_URL);
var SUPPORT_POSTS = SUPPORT_NEWS.child('posts');
var LAST_NEWS_UPDATE = new Firebase(FIREBASE_URL+'updated');
var app = angular.module('qsee',['ui.router','720kb.datepicker','ngFileUpload','firebase','angulartics','angulartics.kissmetrics','angulartics.google.analytics'])  
.controller('search',function($http,$scope,$rootScope,$timeout,$state,$q, Upload){  

  //variable for location so that I can run the same code across various sites
  //and not have to ask... "WHERE AM I?????"  
  var whereAmI = location.protocol +'//'+ location.hostname;

  if(location.hostname ==='localhost'){
    whereAmI = whereAmI+':3000'
  }

  //Hides the development header
  function hideDevHeader(){
    var dev = document.getElementById('rn_DevelopmentHeader');
    if(dev){
      dev.style.display = 'none';
      console.log('development header removed');
    }
  }
  hideDevHeader();
  
  // These are the initial values of various scope variables
  // I put this here so it looks like I know what I am doing

  $scope.youHaveAnOldProduct = false;
  $scope.rmaProofOfPurchase = true;
  $scope.rmaPurchaseLocation = true;
  $scope.rmaPurchaseDate = true;
  $scope.wherePurchasedLocations = undefined;
  $scope.where_purchased = undefined;
  $scope.how_many = 1;
  $scope.form = false;
  $scope.product = undefined;
  $scope.nested_answer_summary = undefined;
  $scope.fileUpload = undefined;
  $scope.fileCleared = false;
  $scope.answers = [];
  $scope.products = [];
  $scope.countries = undefined;
  $scope.incidentSubject = '';
  $scope.incidentInfo = [];
  $scope.states = undefined;
  $scope.date_purchased = undefined;
  $scope.date_purchased_ISO = undefined;
  $scope.within_6_months = undefined;
  $scope.country_id = undefined;
  $scope.contactUpdated = false;
  $scope.contactUpdateError = undefined;
  $scope.state_id = undefined;
  $scope.reasonsReturned = undefined;
  $scope.reason_returned = undefined;
  $scope.searchInputFocused = false;
  $scope.continue = 1;
  $scope.flatProducts = [];
  $scope.creationError = undefined;
  $scope.loginError = undefined;
  $scope.userNameMessage = undefined;
  $scope.passwordResetMessage = undefined;
  $scope.password_reset_error = undefined;
  $scope.categoryHeader = 'Please choose a category or search below';  
  $scope.contact_preference = 1;
  $scope.support_note_page = 0;
  $scope.ans_page = 0;
  $scope.prod_page = 0;
  $scope.caughtUp = true;
  $scope.submitting = false;
  $scope.incident_updated = false;


  //This is to check new updates to make sure that a 
  //customer is up to date on these notifications
  LAST_NEWS_UPDATE.on("value", function(snapshot) {
    
    //scope variable to see when the last update was made on Firebase
    $scope.firebaseLastUpdatedValue = parseInt(snapshot.val());
        
    //check if the last updated item is in localStorage
    if(localStorage.getItem("updated")){
      var lastUpdatedValue = parseInt(localStorage.getItem("updated"));
      if($scope.firebaseLastUpdatedValue > lastUpdatedValue){
        $scope.caughtUp = false;
      }else{
        $scope.caughtUp = true;
      }
    }else{
      $scope.caughtUp = false;
    }
    
  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
  });

  //Function to fix the ui-router links so that they have the proper hash routing link and not some weird URL
  $scope.fixLinks = function(){
    $timeout(function(){
      var srefs = document.querySelectorAll('[ui-sref]');
      for(var i = 0; i < srefs.length; i++){
        if(srefs[i].href != undefined && srefs[i].href.match(/euf/)){
          var a = srefs[i].href.replace(new RegExp(/(\/euf\/rightnow\/optimized\/\d\d\d\d\d\d\d\d\d\d\/themes\/standard)|(\/euf\/assets\/themes\/standard\/)/),'');
          srefs[i].href = a;
        }
      }
    })
  };

  //for pagination, which I don't think I built into the site...
  $scope.numberOfPages=function(array){
    return Math.ceil(array.length/10);                
  }

  // The following are functions for 
  // Handling various forms

  $scope.hideButtonOnSubmitShowWhenCompleted = function(index,uploading){
    $scope.submitting = $scope.submitting == true ? false : true;
    var elementIndex = index == undefined ? 0 : index;
    var button = document.querySelectorAll('[disable-on-submit]');
    var spinner = document.getElementsByClassName('hidden-spinner');
    if(button.length > 0 && spinner.length > 0){
      button[elementIndex].classList.contains('hidden') ? button[elementIndex].classList.remove('hidden') : button[elementIndex].classList.add('hidden');
      if(uploading==undefined){
        spinner[elementIndex].classList.contains('hidden') ? spinner[elementIndex].classList.remove('hidden') : spinner[elementIndex].classList.add('hidden');
      }
    }
  }

  $scope.fieldObjectCreator = function(form,touchedOnly){
    var fieldObject = {};
    angular.forEach(form, function(value, key) {
      if(key[0] == '$') return;
      if(touchedOnly ==true && value.$pristine == false){
        fieldObject[value.$name] = value.$modelValue;
      }else if(touchedOnly==undefined || touchedOnly==false){
        if(value.$modelValue!==undefined){
          fieldObject[value.$name] = value.$modelValue;
        }
      }
    })
    return fieldObject;
  }
  
  //Function for handling logins
  $scope.logInHandler = function(isValid,inOrOut,login,password,redirect){
    $scope.submittedLogin = true;
    $scope.loginError = undefined;  
    function postLogin(url){
        $http({
          method: 'POST',
          url: url,

          //set the headers to be the in the correct format for doing a POST request
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function(obj) {
              var str = [];
              for(var p in obj)
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
          },

          //Set up the data accordingly
          data: {login:login, password:password,redirect:redirect}
        }).then(function successCallback(response) {
          //if there is an error, make sure to list it otherwise determine if the user is logging in or logging out
          //and act accordingly; also, if they have a page they need to redirect to go there instead of the account page
              if(response.data.success == 0){
                $scope.loginError = response.data.message;
                $scope.hideButtonOnSubmitShowWhenCompleted();  
              }else{
                $timeout(function(){
                  if(inOrOut=='in'){
                    if(redirect!=undefined){
                      window.location = window.location.origin+'/app/#/'+redirect;  
                    }else{
                      location.reload()
                    }
                  }else if(inOrOut=='out'){
                    window.location = window.location.origin;
                  }
                });
              }
          }, function errorCallback(response) {  
          //if there is an error, tell me what it is
          console.log(response)          
        });
    }
    if(isValid && inOrOut =='in'){
      $scope.hideButtonOnSubmitShowWhenCompleted();
      postLogin(whereAmI+'/cc/api/doLogin');  
    }
    else if(inOrOut=='out'){
        postLogin(whereAmI+'/cc/api/doLogout');
    }
  }
  

  //account creation
  $scope.createAccount = function(isValid,email,first_name,last_name,password_new,password_verify){
      $scope.submitted = true;
      if (isValid && password_new==password_verify) {
        $scope.hideButtonOnSubmitShowWhenCompleted();
        $http({
          method: 'POST',
          url: whereAmI+'/cc/api/createAccount',
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          transformRequest: function(obj) {
              var str = [];
              for(var p in obj)
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
          },
          data: {email:email,firstname:first_name,lastname:last_name,password:password_new}
        }).then(function successCallback(response) {
            if(response.data.success ==0){
              $scope.creationError = response.data.message; 
            }else{
              $timeout(function(){
                $scope.logInHandler(true,'in',email,password_new,'account_overview');
              })
            }
            $scope.submitted = false;  
            $scope.hideButtonOnSubmitShowWhenCompleted();          
          }, function errorCallback(response) {  
          console.log(response)          
        });
      }
  } 
  // uploading files

  $scope.uploadFile = function(file) {
    if(file!= undefined || file!= null){
      $scope.fileCleared = false;
      $scope.hideButtonOnSubmitShowWhenCompleted(0,true);
      file.upload = Upload.upload({
        url: whereAmI+'/ci/fattach/upload',
        data: { file: file},
      });

      file.upload.then(function (response) {
        $timeout(function () {
          $scope.uploaded_file = response.data;  
          $scope.hideButtonOnSubmitShowWhenCompleted(0,true);
        });
      }, function (response) {
          if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
          file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          if(file.progress == 100){
            
          }
        });
    }   
  }

  //clears the file

  $scope.clearFile = function(){
    $scope.fileUpload = undefined;
    $scope.uploaded_file = undefined;
    $scope.fileCleared = true;
  }

  //Function for syncing product registration data on RMA form

  $scope.productRegistrationSync = function(data){
    if(data=='clear'){
      $scope.where_purchased = undefined;
      $scope.date_purchased = undefined;
      $scope.rmaProofOfPurchase = true;
      $scope.rmaPurchaseLocation = true;
      $scope.rmaPurchaseDate = true;
      $scope.file_upload = undefined;
      $scope.prodRegIncidentNo = undefined;
    }else{
      $scope.file_upload = undefined;
      var moddedType = JSON.parse(data);
      if(moddedType.proof_of_purchase == 'true'){  
        $scope.rmaProofOfPurchase = false;
      }
      if(moddedType.purchase_location_id!=undefined && $scope.wherePurchasedLocations!=undefined){
        var pl_id = parseInt(moddedType.purchase_location_id);
        $scope.where_purchased = pl_id;
        $scope.rmaPurchaseLocation = false;
      }
      if(moddedType.purchase_date!=undefined){
        var purchase_date = new Date(moddedType.purchase_date);
        $scope.date_purchased = purchase_date.toISOString(); 
        $scope.rmaPurchaseDate = false;
      }
      if(moddedType.product_id!=undefined){
        $scope.prodId = moddedType.product_id;
      }
      if(moddedType.reference_no!=undefined){
        $scope.prodRegIncidentNo = moddedType.reference_no;
      }
    }
  }
  
  //function for testing chat
  $scope.launchChat = function(formData,contact_info,currentProduct){
      if(formData.$valid){
        $scope.hideButtonOnSubmitShowWhenCompleted();
        var url = whereAmI+'/app/chat/chat_landing/subject/'+formData.subject.$modelValue+'/prod/'+currentProduct+'/first_name/'+contact_info.first_name+'/last_name/'+contact_info.last_name+'/email/'+contact_info.email;
        window.location = url;
      }
  }

  //Function for submitting incidents...
  $scope.submitIncident = function(isValid, incidentType,form,product_id,file_upload,contact_preference){  
    $scope.submitted = true;

    function dateCreator(dateInStringFormat){
        var makeDate = new Date(dateInStringFormat);
        return makeDate.toISOString()
    }
    if(isValid && $scope.submitting == false){  
      $scope.hideButtonOnSubmitShowWhenCompleted();
      // instantiate an empty object for collecting the form data
      var fieldObject = $scope.fieldObjectCreator(form);

      // if there is a fileupload, make sure that it is parsed together the right way

      if(file_upload){
        fieldObject['file_upload_tmp_name'] = file_upload.tmp_name;
        fieldObject['file_upload_name'] = file_upload.name
      }

      // set the contact preference for the product registration forms

      if(contact_preference){
        fieldObject['contact_preference'] = contact_preference; 
      }

      if(fieldObject['date_purchased']){
        fieldObject['date_purchased'] = new Date(fieldObject['date_purchased']).toISOString();
      }

      // set some special fields for the various cases

      if(incidentType=='productRegistration'){
        fieldObject['prod_reg'] = 1;
      }else if(incidentType=='rmaIncident'){
        fieldObject['rma'] = 1;
        var summary = [$scope.rmaForm.product.$modelValue,$scope.incidentSubject,$scope.rmaForm.subject.$modelValue]; 
        var parsedSummary = [];
        for(var i = 0; i < summary.length; i++){
          if(summary[i]!==undefined && summary[i].length!==0){
          	summary[i] = summary[i].replace(/\n/g, '');
            parsedSummary.push(summary[i]);
          }
        }
        fieldObject['summary'] = parsedSummary.join(' . ');
        if($scope.prodRegIncidentNo){
          fieldObject['product_registration_incident_no'] = $scope.prodRegIncidentNo;
        }
      }else{
        var summary = [$scope.tsForm.product.$modelValue,$scope.incidentSubject,$scope.tsForm.subject.$modelValue]; 
        var parsedSummary = [];
        for(var i = 0; i < summary.length; i++){
          if(summary[i]!==undefined && summary[i].length!==0){
          	summary[i] = summary[i].replace(/\n/g, '');
            parsedSummary.push(summary[i]);
          }
        }
        fieldObject['summary'] = parsedSummary.join(' . ');
      }

      fieldObject['product_id'] = product_id;
      
      $http({
        method: 'POST',
        url: whereAmI+'/cc/api/createIncident',

        //set the headers to be the in the correct format for doing a POST request
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        transformRequest: function(obj) {
            var str = [];
            for(var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            return str.join("&");
        },
        // send the fieldObject data
        data:fieldObject
      }).then(function successCallback(response) {    

            if(response.data.success == 1){
              if(incidentType == 'productRegistration'){
                $scope.contact_info.prod_reg_incidents.unshift(response.data.incident);
                $state.go('product_registration_confirmation',{i_id: response.data.incident.i_id});
              }else if(incidentType == 'rmaIncident'){
                $scope.contact_info.rma_incidents.unshift(response.data.incident);
                $state.go('rma_confirmation',{i_id: response.data.incident.i_id});
              }else{
                $scope.contact_info.incidents.unshift(response.data.incident);
                $state.go('incident_confirmation',{i_id: response.data.incident.i_id});
              }
            }else{
              $scope.hideButtonOnSubmitShowWhenCompleted();
              $scope.incidentError = 'There was an error with your incident, please try again later';
            }
        }, function errorCallback(response) {  
        //if there is an error, tell me what it is
          console.log(response);
        
      });
    }
  }

  $scope.setIncidentInfo = function(incidentObject){
  	$scope.incidentInfo = incidentObject;
  }

  $scope.updateIncident = function(form,file_upload){
    $scope.submitted = true;
    $scope.incidentError = undefined;
    if(form.$valid && $scope.submitting !=true){
      $scope.hideButtonOnSubmitShowWhenCompleted();
      var fieldObject = $scope.fieldObjectCreator(form);
      if(file_upload && file_upload.tmp_name && file_upload.name){
        fieldObject['file_upload_tmp_name'] = file_upload.tmp_name;
        fieldObject['file_upload_name'] = file_upload.name
      }
      $timeout(function(){
        $http({
          method: 'POST',
          url: whereAmI+'/cc/api/updateIncident',

          //set the headers to be the in the correct format for doing a POST request
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function(obj) {
              var str = [];
              for(var p in obj)
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
          },
          // send the incidentData object
          data:fieldObject
        }).then(function successCallback(response) {
            if(response.data.success == 1){
              $scope.incidentInfo = response.data.incidentInfo;
              $scope.incident_updated = true;  
              $scope.fixLinks();
            }else{
              $scope.incidentError = response.data.incidentError;
            }
            $scope.hideButtonOnSubmitShowWhenCompleted();
          }, function errorCallback(response) {  
          //if there is an error, tell me what it is
            console.log(response);
        })
      },1000)
    }
  }

  //answer search
  $scope.answerSearch = function(query,clearpages){
    var prodQuery = '';
    var answerQueryArray = [];
    document.querySelectorAll('input.search-input')[0].blur();
    $scope.searching = 'Searching';
    if(query!=undefined || query == ''){
      var queryArray = query.toLowerCase().split(' ');
      for(var i = 0; i < queryArray.length; i++){
        if(queryArray[i].match(/((^(q|b|dv|re|hs)[a-z]| ){0,6}\d{0,5})/igm) && queryArray[i]!="backup" && queryArray[i]!="back"){  
          prodQuery = queryArray[i];
        }else{
          answerQueryArray.push(queryArray[i]);
        }
      }
    }
    var answerQuery = answerQueryArray.join(' ') || '';
    if(clearpages){
      var clear_pages = clearpages;
    }
    var searchQuery = query;
    $scope.queryAnswers(searchQuery,clear_pages);
    $scope.queriedProducts = [];
    function generateProductsResults(){
      var queryRegex = new RegExp(prodQuery.split(/[-]/)[0],'igm'); 
      angular.forEach($scope.flatProducts,function(product){
        if(product['name'].match(queryRegex)){
          this.push(product);
        }
      },$scope.queriedProducts);
    }
    if(prodQuery!=undefined){
      if($scope.products.length != 0){
        generateProductsResults();
      }else{
        $timeout(function(){
        generateProductsResults();
        },2500)
      }
    }
  }

  // This is for the RMA form, product registration form, and the account settings update form
  // so that we can have a state change 
  // when the form hasn't been touched
  // but if it has, we only send a request to update the fields that were touched

  $scope.contactInfoUpdate = function(form,currentPage,whereTo){
    $scope.submitted = true;
    if(form.$valid && $scope.submitting == false){
      var fieldObject = $scope.fieldObjectCreator(form,true);
      if(Object.keys(fieldObject).length == 0){
        if(whereTo != undefined){
          $state.go(whereTo);
        }
      }else{
        $scope.hideButtonOnSubmitShowWhenCompleted();
        
        $http({
          method: 'POST',
          url: whereAmI+'/cc/api/updateAccount',

          //set the headers to be the in the correct format for doing a POST request
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function(obj) {
              var str = [];
              for(var p in obj)
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
          },
          // send the incidentData object
          data:fieldObject
        }).then(function successCallback(response) { 
            if(response.data.error){
              $scope.contactUpdateError = response.data.error;
            }
            if(response.data.contact_info){
              $scope.contact_info = JSON.parse(response.data.contact_info);
            }   
            if(response.data.account_updated == true && whereTo!==undefined){
              $state.go(whereTo);
            }else if(response.data.account_updated == true && whereTo==undefined){
              $scope.contactUpdated = true;
            }
          }, function errorCallback(response) {  
          //if there is an error, tell me what it is
            console.log(response);
        }).then(function(){
            $scope.hideButtonOnSubmitShowWhenCompleted();
        });
      }
    }
  }

  //function for sending answer feedback
  $scope.sendAnswerFeedback = function(a_id,rating){
    $http({
      method: 'POST',
      url: whereAmI+'/ci/ajaxRequest/submitAnswerRating',

      //set the headers to be the in the correct format for doing a POST request
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      transformRequest: function(obj) {
          var str = [];
          for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
      },
      // send the incidentData object
      data:{a_id: a_id,options_count: 2,rate: rating,threshold: 1}
    }).then(function successCallback(response) {    
          $scope.$$childTail.active = true;
      }, function errorCallback(response) {  
      //if there is an error, tell me what it is
        console.log(response);
    })
  }

  // function for requesting username
  // with this rewrite, it should be the same as emails
  // but let's not assume
  // that this is the case for all of our users

  $scope.requestUserName = function(isValid,email){
    $scope.submittedUsernameRequest = true;
    if(isValid){
      $scope.hideButtonOnSubmitShowWhenCompleted();
      $http({
          method: 'POST',
          url: whereAmI+'/cc/api/emailUsername',

          //set the headers to be the in the correct format for doing a POST request
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function(obj) {
              var str = [];
              for(var p in obj)
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
          },

          //Set up the data accordingly
          data:{email:email}
        }).then(function successCallback(response) {
            $scope.userNameMessage = response.data.message;
            $scope.hideButtonOnSubmitShowWhenCompleted();  
          }, function errorCallback(response) {  
          //if there is an error, tell me what it is
            console.log(response);
          
        });
    
    }
  }

  // function for requesting a password reset while not logged in

  $scope.resetPasswordRequest = function(isValid,login){
    $scope.submittedPasswordReset = true;
    if(isValid){
      $scope.hideButtonOnSubmitShowWhenCompleted(1);
      $http({
          method: 'POST',
          url: whereAmI+'/cc/api/emailPassword',

          //set the headers to be the in the correct format for doing a POST request
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function(obj) {
              var str = [];
              for(var p in obj)
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
          },

          //Set up the data accordingly
          data:{login:login}
        }).then(function successCallback(response) {

            $scope.passwordResetMessage = response.data.message;
            $scope.password_reset_error = response.data.password_reset_error;
            $scope.hideButtonOnSubmitShowWhenCompleted(1);
              
          }, function errorCallback(response) {  
          //if there is an error, tell me what it is
            console.log(response);
          
        });
    
    }
  }

  // function for resetting passwords for customers
  // by email
  $scope.resetPassword = function(isValid,cred,password_new, password_verify){
    $scope.submittedPasswordReset = true;
    if(isValid && cred){
      $scope.hideButtonOnSubmitShowWhenCompleted();
      $http({
          method: 'POST',
          url: whereAmI+'/cc/api/resetPassword',

          //set the headers to be the in the correct format for doing a POST request
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function(obj) {
              var str = [];
              for(var p in obj)
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
          },

          //Set up the data accordingly
          data:{pw_reset:cred,password_new:password_new,password_verify:password_verify}
        }).then(function successCallback(response) {
          $scope.hideButtonOnSubmitShowWhenCompleted();
            if(response.data.success == 1){
              window.location = whereAmI+'/app/#/reset_password_confirmation';  
            }
              
          }, function errorCallback(response) {  
          //if there is an error, tell me what it is
            console.log(response);
          
        });
    }
  }

  //function for changing customer portal password for a contact
  $scope.changePassword = function(form){
    $scope.submitted=true;  
    if(form.$valid){
      $scope.password_reset_error =undefined;
      $scope.hideButtonOnSubmitShowWhenCompleted();
      var fieldObject = {};
      angular.forEach(form, function(value, key) {
        if(key[0] == '$') return;
        if(value.$modelValue!==undefined){
          fieldObject[value.$name] = value.$modelValue;
        }
      });

      $http({
          method: 'POST',
          url: whereAmI+'/cc/api/changePassword',
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          transformRequest: function(obj) {
              var str = [];
              for(var p in obj)
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
          },
          data: fieldObject
        }).then(function successCallback(response) {
            if(response.data.password_reset_error){
              $scope.password_reset_error = response.data.password_reset_error;
            }
            $scope.hideButtonOnSubmitShowWhenCompleted();
            if(response.data.success == 1){
              $state.go('reset_password_confirmation');
            }
          }, function errorCallback(response) {  
          console.log(response)          
        });
    }
  }

  // These for tracking the inputted date
  // and to make sure that today
  // really is today
  // calling today using scope
  // is a recipe for disaster
  // so I must pass in today through a reference

  var today = new Date();


  $scope.today = function(setting){
    return today.toISOString();
  };

  $scope.date = function(dateString){

    //stole this from 
    //http://stackoverflow.com/questions/476105/how-can-i-convert-string-to-datetime-with-format-specification-in-javascript#answer-7712335
    //using this on the incident show pages for the thread dates
    //tried a bunch of PHP date time conversions...
    //hade to use this...
    var reggie = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
    var dateArray = reggie.exec(dateString); 
    var dateObject = new Date(
        (+dateArray[1]),
        (+dateArray[2])-1, // Careful, month starts at 0!
        (+dateArray[3]),
        (+dateArray[4]),
        (+dateArray[5]),
        (+dateArray[6])
    );
    return dateObject;
  }

  $scope.compareDates = function(today, selectedDate){
    var todaysDate = new Date(today);
    var dateToCompare = new Date(selectedDate);
    return todaysDate < dateToCompare;
  }
  
  //do not define these, that will break state
  $rootScope.previousState;
  $rootScope.currentState;

  //below is the set up and teardown for web workers; really boosts the performance on mobile and desktop

  function callWebWorker (url) {
      var worker = new Worker(whereAmI+'/euf/assets/themes/qsee/ajaxWebWorker.js');
      var defer = $q.defer();
      worker.onmessage = function(e) {           
        defer.resolve(e.data);
        worker.terminate();
    };

    worker.postMessage(url);
    return defer.promise;
  }
  
  $scope.getAllProducts = function(){
    callWebWorker(whereAmI+"/cc/api/getAllProducts").then(function (workerReply) {
      $scope.products = JSON.parse(workerReply);
      angular.forEach($scope.products, function(key,value){
        angular.forEach($scope.products[value],function(product){
          if(value!=='Accessories'){
            this.push({'name':product.name,'technology':product.technology});
          }else{
            this.push({'name':product.name});
          }
        },$scope.flatProducts)
      });
    });
  }
  $scope.getProduct = function(product){
    callWebWorker(whereAmI+"/cc/api/getProductInfo?product="+product).then(function (workerReply) {
      $scope.product = JSON.parse(workerReply);
    });
  }
  $scope.getAnswers = function(){
    callWebWorker(whereAmI+"/cc/api/getAllAnswers").then(function (workerReply) {
      $scope.answers = JSON.parse(workerReply);
    });
  }
  $scope.getOneAnswer = function(a_id,nested){
    var url = nested==true ? "/cc/templates/answer?a_id="+a_id+"&nested=true" : "/cc/templates/answer?a_id="+a_id;
    callWebWorker(whereAmI+url).then(function (workerReply) {
      $scope.selected_answers = undefined;
      $scope.categoryHeader = undefined;
      $scope.nested_answer_summary = workerReply;
      $timeout(function(){
        smoothScroll('answer_list');
      },100)
    });
  }
  $scope.getCountries = function(){
    if($scope.countries ==undefined){
      callWebWorker(whereAmI+"/cc/api/getCountries").then(function (workerReply) {
        $scope.countries = JSON.parse(workerReply);
      });
    }
  }
  $scope.getStates = function(){
    if($scope.states ==undefined){
      callWebWorker(whereAmI+"/cc/api/getStates").then(function (workerReply) {
        $scope.states = JSON.parse(workerReply);
      });
    }
  }
  $scope.getWherePurchased = function(){
    if($scope.wherePurchasedLocations == undefined){
      callWebWorker(whereAmI+"/cc/api/getWherePurchased").then(function (workerReply) {
        $scope.wherePurchasedLocations = JSON.parse(workerReply);
      });
    }
  }
  $scope.getReasonsReturned = function(){
    if($scope.reasonsReturned == undefined){
      callWebWorker(whereAmI+"/cc/api/getReasonsReturned").then(function (workerReply) {
        $scope.reasonsReturned = JSON.parse(workerReply);
      });
    }
  }
  $scope.setToken = function(){
    callWebWorker(whereAmI+"/cc/api/set_ftok").then(function (workerReply) {
    });
  }
  $scope.queryAnswers = function(query,clearpages){
    callWebWorker(whereAmI+"/cc/api/getAnswers?query="+query).then(function (workerReply) {
      $scope.searchInputFocused = false;
      $scope.searching = undefined;
      $scope.queriedAnswers = JSON.parse(workerReply);
      if($scope.queriedAnswers!==undefined){
        $state.go('search',{ query: query});
      }
      $scope.fixLinks();
    });
  }

  // These are utility functions for setting the scope values
  // for certain fields
  $scope.setScopeValue = function(checkForContact,scopeVar, value){
    $timeout(function(){
      if(checkForContact == true){
        if($scope.contact_info!=undefined){
          $scope[scopeVar] = value;
        }
      }else if(checkForContact == false && value!==undefined){
        $scope[scopeVar] = value;
      }
    },1000)
  }

  $scope.setCountry = function(id){
    $timeout(function(){
      if(id){
        $scope.country_id = parseInt(id);
      }
    },1000)
  }

  $scope.setState = function(id){
    $timeout(function(){
      if(id){
        $scope.state_id = parseInt(id);
      }
    },1000)
  }

  //utility function for the product pages

  $scope.setSelected = function(input,answerCategory,answerSubCategory,answerSubSubCategory){
    if(input!==undefined){
      if(input.length > 1){
        $scope.selected_answers = input;
        $scope.answerCategory = answerCategory;
        $scope.answerSubCategory = answerSubCategory;
        $scope.answerSubSubCategory = answerSubSubCategory;
        $timeout(function(){
          smoothScroll('answer_list');
        },100)
        $scope.nested_answer_summary = undefined;
      }else if(input.length ==1){
        $scope.getOneAnswer(input[0].id,true);        
      }else{
        $scope.nested_answer_summary = undefined;
      }
    }
    $scope.fixLinks();  
  }

  // function for scrolling to the top of the page
  // original put this in the rootscope function below
  // but there were some issue
  $scope.scrollToTheTop = function(to, from){
    if(to.name == 'home' ||
      (from.name.indexOf('product') == 0 && to.name.indexOf('product') == 0) ||
      (from.name.indexOf('answers') == 0 && to.name.indexOf('answers') == 0) ||
      to.name.indexOf(from.name.split('_')[0]) !== 0 ||
      (from.name.indexOf('rma_') == 0 && to.name.indexOf('rma_') == 0)){
        document.getElementsByTagName('body')[0].scrollTop = '0px';
        document.getElementsByTagName('html')[0].scrollTop = '0px'; 
    }
  }


  // this is a utility for tracking the app state
  // it also does the following: 
  // reinitializes certain scope parameters

  $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
      $scope.scrollToTheTop(to,from);
      $timeout(function(){
        $scope.fixLinks();
      },500);
      $scope.within_6_months = undefined;
      $scope.date_purchased = undefined;
      $scope.date_purchased_ISO = undefined;
      $scope.searching = undefined;
      $scope.submitted = false;
      $scope.submittedPasswordReset = false;
      $scope.submittedUsernameRequest = false;
      $scope.currentProduct = undefined;
      $scope.creationError = undefined;
      $scope.loginError = undefined;
      $scope.userNameMessage = undefined;
      $scope.passwordResetMessage = undefined;
      $scope.password_reset_error =undefined;
      $scope.categoryHeader = 'Please choose a category or search below';  
      $scope.rmaProofOfPurchase = true;
      $scope.rmaPurchaseLocation = true;
      $scope.rmaPurchaseDate = true;
      $rootScope.previousState = from.name;
      $rootScope.currentState = to.name;
      $scope.contactUpdated = false;
      $scope.answerCategory = undefined;
      $scope.answerSubCategory = undefined;
      $scope.answerSubSubCategory = undefined;
      $scope.submitting = false;
      $scope.incident_updated = false;
      $scope.contactUpdateError = undefined;
      $timeout(function(){
        $scope.detectStateForTokens();
      })
      $scope.clearFile();     
      if($scope.productStateCheck() == true || $scope.detectLastState('answers')){
        $scope.nested_answer_summary = undefined;
      }
  });


      // Utility Functions
      // for Hiding/Showing certain elements
      // depending on the state
      // You should probably leave these alone...

      //function for the searchbar to float up, but only if it is being accessed from the support home page
      $scope.moveMeIfICameFromHome = function(){
        if($rootScope.previousState =='home'){
          return true;
        }
      }

      //function for the searchbar to float down if it is going back to the home page from the search page
      $scope.moveMeBack = function(){
        if($rootScope.previousState =='search' && $rootScope.currentState =='home' ){
          return true;
        }
      }

      //function for hiding the Login link when on the login page
      $scope.hideOnLoginPage = function(){
        if($rootScope.currentState !='login_page' && $rootScope.currentState !='account_creation'){
          return true;
        }
      }

      //function for no fading between the account creation and login page
      $scope.stopFading = function(){
        if($rootScope.previousState =='login_page' || $rootScope.previousState =='account_creation'){
          return false;
        }else{
          return true;
        }
      }

      //function to handle the rma_form pages
      $scope.rmaFormHandling = function(){
        if($rootScope.currentState =='rma_form_2' && $scope.contact_info !=undefined){
          return true;
        }else{
          return false;
        }
      }

      //function for checking if the last page was the account page or the rma_home
      $scope.rmaListButton = function(checkLastPage){
        if($rootScope.previousState =='rma_home' && checkLastPage == 'rma_home'){
          return true;
        }else if($rootScope.previousState =='account_overview.rmas' && checkLastPage == 'account_overview.rmas'){
          return true;
        }else if(($rootScope.previousState == '' || $rootScope.previousState == 'incident_show') && checkLastPage == 'blank'){
          return true;
        }else{
          return false;
        }
      }

      //function for the login button to look active on the login page
      $scope.accountSubmit = function(state){
        if(state =='product_registration' && $rootScope.currentState =='product_registration'){
          return true;
        }
        else if(state =='rma_form' && $rootScope.currentState =='rma_form'){
          return true;
        }
        else if(state =='account_settings' && $rootScope.currentState =='account_settings'){
          return true;
        }
      } 
  

      //function to hide the account name (amongst other things) when on the account page (if logged in)
      $scope.onAccountPage = function(nested_state){
        if(nested_state == 'incidents'){
          if($rootScope.currentState == 'account_overview.incidents'){
            return true;
          }
        }else if(nested_state == 'rmas'){
          if($rootScope.currentState == 'account_overview.rmas'){
            return true;
          }
        }else if(nested_state == 'registered_products'){
          if($rootScope.currentState == 'account_overview'){
            return true;
          }
        }else{
          if($rootScope.currentState && $rootScope.currentState.match(/account_overview/)){
            return true;
          }
        }

      } 

      $scope.productStateCheck = function(){
        if($rootScope.previousState =='product'){
          return true;
        } 
      }

      // Generic State Testing function
      // to show or hide elements
      
      $scope.detectState = function(state){
        if($rootScope.currentState ==state){
          return true;
        } 
      }

      // Generic State Testing function
      // to show or hide elements
      
      $scope.detectLastState = function(state){
        if($rootScope.previousState ==state){
          return true;
        } 
      }

      // modified version of the above function
      // for the product show
      // using it for styling purposes
      
      $scope.detectLastStateLike = function(state){
        if($rootScope.previousState.slice(-1) !== 's' &&$rootScope.previousState.indexOf(state.split('_')[0]) == 0){
          return true;
        } 
      }

      // State Testing function
      // for handling tokenization
      // is run on the rootScope function
      // whenever a state change occurs
      
      $scope.detectStateForTokens = function(){  
        if(($rootScope.currentState == 'rma_form_2' || 
          $rootScope.currentState == 'product_registration_' || 
          $rootScope.currentState =='ask_a_question' || 
          $rootScope.currentState =='reset_password') && ($rootScope.previousState!= undefined && !$rootScope.previousState.match(/confirmation/))){
          $scope.setToken();
        } 
      }

      //function for selecting a product for the product drop-down
      $scope.setProduct = function(showOrHide,prod,id){
        if(showOrHide=='show'){
          $scope.currentProduct = prod;
          $scope.prodId = id;
        }else if(showOrHide=='hide'){
          $scope.currentProduct = undefined;
          $scope.prodId = undefined;
        }
      }

      // initialization for the app
      // may need to throw this into a $q.all service and refactor the code above

      $scope.getAnswers();
      $scope.getAllProducts();
      $scope.getReasonsReturned();
      $scope.getCountries();
      $scope.getStates();
      $scope.getWherePurchased();

      $scope.$watch('contact_info', function() {  

      }, true);
      $scope.$watch('firebaseLastUpdatedValue', function() {  

      }, true);
})
.config(function($stateProvider, $urlRouterProvider,$locationProvider,$analyticsProvider) {

  // $locationProvider.html5Mode({enabled: true,requireBase: false});

  // For any unmatched url, redirect to root
  $urlRouterProvider.otherwise("/");
  //
  // Now set up the states
  $stateProvider
    .state('home', {
      url: "/",   
      templateUrl: "/euf/assets/themes/qsee/partials2/search_index.html",
    })
    .state('error', {
      url: "/error",   
      templateUrl: "/euf/assets/themes/qsee/partials2/error.html",
    })
    .state('remote_rescue', {
      url: "/remote_rescue",   
      templateUrl: "/euf/assets/themes/qsee/partials2/remote_rescue.html",
    })
    .state('support_notifications', {
      url: "/support_notifications",   
      templateUrl: "/euf/assets/themes/qsee/partials2/support_notifications.html",
      controller:function($scope,$firebaseArray){
        $scope.posts = $firebaseArray(SUPPORT_POSTS);
      }
    })
    .state('support_notification', {
      url: "/support_notification?id",   
      templateUrl: "/euf/assets/themes/qsee/partials2/support_notifications.html",
      controller:function($scope,$firebaseObject,$stateParams,$timeout){
        if($stateParams.id){
          var postUrl = $firebaseObject(SUPPORT_POSTS.child($stateParams.id));
          $scope.post = postUrl.$$conf.binding.rec;
          $timeout(function(){
              var postUpdatedTime = $scope.post.updated;
              var postCreatedTime = $scope.post.created_time;
              if(postCreatedTime >= $scope.firebaseLastUpdatedValue || (postUpdatedTime!== undefined && postUpdatedTime >= $scope.firebaseLastUpdatedValue)){
                $scope.caughtUp = true;
                localStorage.setItem("updated",$scope.firebaseLastUpdatedValue);
              }
          },1000)
        }
      }
    })
    .state('search', {
      url: "/search?query",   
      templateUrl: "/euf/assets/themes/qsee/partials2/search_index.html",
      controller: function($scope,$stateParams,$timeout){
        $timeout(function(){
          $scope.incrementPage = function(scopeVar){
            $timeout(function(){
              $scope[scopeVar] = $scope[scopeVar] + 1;
              smoothScroll(scopeVar);
            },1)
          }
          $scope.decrementPage = function(scopeVar){
            $timeout(function(){
              $scope[scopeVar] = $scope[scopeVar] - 1;
              smoothScroll(scopeVar);
            },1)
          }
          $scope.query = $stateParams.query!== undefined? $stateParams.query.trim() : '';  
          if($scope.query && $scope.query.length!==0 && ($scope.queriedAnswers==undefined || $scope.queriedAnswers.length == 0 )){
            $scope.answerSearch($stateParams.query);
          }
        })
      }
    })
    .state('ask_a_question', {
      url: "/ask_a_question?contact_method&prod",   
      templateUrl: "/euf/assets/themes/qsee/partials2/ask_a_question.html",
      controller: function($scope,$stateParams,$timeout){
        $scope.contact_method = $stateParams.contact_method;
        $scope.currentProduct = $stateParams.prod;
        $scope.updateTextArea = function(subject_prerendered){
          var checked = [];
          var checkedBoxes = document.querySelectorAll(".taglist :checked");
          for (var i = checkedBoxes.length - 1; i >= 0; i--) {
            checked.push(checkedBoxes[i].value)
          };
          var checkedSubject = checked.join(' . ');
          if(subject_prerendered!==undefined){
            $scope.subject = checkedSubject +' '+subject_prerendered.replace(/\n/g, '');
          }
        } 
      }
    })
    .state('answer', {
      url: "/answer?a_id",   
      templateUrl: function($stateParams){
        return "/cc/templates/answer?a_id="+$stateParams.a_id
      }
    })
    .state('account_settings', {
      url: "/account_settings",   
      templateUrl: "/euf/assets/themes/qsee/partials2/account_settings.html",
      controller: function($scope,$stateParams,$timeout){
      
      }
    })
    .state('account_assistance', {
      url: "/account_assistance",   
      templateUrl: "/euf/assets/themes/qsee/partials2/account_assistance.html",
      controller: function($scope,$stateParams,$timeout){
      
      }
    })
    .state('account_overview.incidents', {
        url: "_incidents", 
        templateUrl: "/euf/assets/themes/qsee/partials2/account_overview.html",
        controller:function($scope,$state){
          
        }
      })
      .state('account_overview.rmas', {
        url: "_rmas", 
        templateUrl: "/euf/assets/themes/qsee/partials2/account_overview.html",
        controller:function($scope,$state){
          
        }
      })
    .state('reset_password', {
      url: "/reset_password?cred",   
      templateUrl: function($stateParams){
        return "/cc/templates/reset_password?cred="+$stateParams.cred
      },
      controller: function($scope,$stateParams,$timeout,$state){
        if($stateParams.cred!=undefined){
          $scope.cred = $stateParams.cred;  
        }
        $scope.passwordResetFix = function(){
          $timeout(function(){
            $scope.setToken();
            if($scope.contact_info!=undefined && $scope.cred!=undefined){
              window.location = '/app/#/reset_password';
            };
          })
        }
      }
    })
    .state('reset_password_confirmation', {
      url: "/reset_password_confirmation",   
      templateUrl: function($stateParams){
        return "/cc/templates/reset_password"  
      }
    })
    .state('product_registration', {
      url: "/product_registration",   
      templateUrl: "/euf/assets/themes/qsee/partials2/product_registration.html",
      controller: function($scope,$stateParams,$timeout){
        
      }
    })
    .state('product_registration_', {
      url: "/product_registration_",   
      templateUrl: "/euf/assets/themes/qsee/partials2/product_registration.html",
      controller: function($scope,$stateParams,$timeout){
        
      }
    })
    .state('answers', {
        url: "/answers?category&cat_lvl_2&cat_lvl_3&prod_lvl_1&prod_lvl_2&prod_lvl_3",   
        templateUrl: "/euf/assets/themes/qsee/partials2/answer_index.html",
          controller: function($scope,$stateParams,$timeout){

            var setLocalScope = function(urlParam){
              if($stateParams[urlParam]){
                $scope[urlParam] = decodeURIComponent($stateParams[urlParam]); 
              }
            }
            
            setLocalScope('category');
            setLocalScope('prod_lvl_1');
            setLocalScope('prod_lvl_2');
            setLocalScope('prod_lvl_3');
            setLocalScope('cat_lvl_2');
            setLocalScope('cat_lvl_3');

            $scope.userDocTest = function(cat_lvl_2,category){
              if(cat_lvl_2!=undefined && category=='How-tos'){
                return false
              }else{
                return true
              }
            }

            $scope.categoryFilter = function(item){
              if($scope.category!=undefined){
                if(item.Category == $scope.category){
                  return item;
                }
              }
            }
            $scope.prod_lvl_1_filter = function(item){
              if($scope.category != undefined && item.Category == $scope.category && item.prod_lvl_1 == $scope.prod_lvl_1){
                return item;
              }
            }
            $scope.prod_lvl_2_filter = function(item){
              if($scope.category != undefined && item.Category == $scope.category && item.prod_lvl_1 == $scope.prod_lvl_1 && item.prod_lvl_2 == $scope.prod_lvl_2){
                return item;
              }
            }
            $scope.prod_lvl_3_filter = function(item){
              if($scope.category != undefined && item.Category == $scope.category && item.prod_lvl_1 == $scope.prod_lvl_1 
                && item.prod_lvl_2 == $scope.prod_lvl_2 && item.prod_lvl_3 == $scope.prod_lvl_3){
                return item;
              }
            }
            $scope.cat_lvl_2_filter = function(item,howToFilter){
              if($scope.category != undefined && item.Category == $scope.category && item.prod_lvl_1 == $scope.prod_lvl_1 
                && item.prod_lvl_2 == $scope.prod_lvl_2 && item.cat_lvl_2 == $scope.cat_lvl_2){
                if(howToFilter!==true){
                  return item;
                }else if(howToFilter== true && item.cat_lvl_3!==''){
                  return item;
                }
              }
            }
            $scope.cat_lvl_3_filter = function(item){
              if($scope.category != undefined && item.Category == $scope.category && item.prod_lvl_1 == $scope.prod_lvl_1 
                && item.prod_lvl_2 == $scope.prod_lvl_2 && item.cat_lvl_2 == $scope.cat_lvl_2 && item.cat_lvl_3 == $scope.cat_lvl_3){
                return item;       
              }
            }
          }
      })
    .state('products', {
      url: "/products?key&series&technology", 
      templateUrl: "/euf/assets/themes/qsee/partials2/product_index.html",
      controller: function($scope,$stateParams){

        $scope.checkProductKey = function(){
          if($stateParams.key){
            $scope.prodCategory = decodeURIComponent($stateParams.key);
            $scope.hideKey = true;
          }
          if($stateParams.technology){
            $scope.prodTechnology = decodeURIComponent($stateParams.technology); 
          }
          if($stateParams.series){
            $scope.prodSeries = decodeURIComponent($stateParams.series); 
          }
        }
        $scope.filteredProducts = function(item){
          if($scope.prodTechnology!= undefined && item.technology==$scope.prodTechnology){
            return item
          }else if($scope.prodSeries!= undefined && item.series==$scope.prodSeries){
            return item
          }
        };
      }
    })
    .state('product', {
      url: "/product?product&cat_lvl_1&cat_lvl_2&cat_lvl_3&cat_lvl_4", 
      templateUrl: function($stateParams){
        var link = "/cc/templates/product?product="+$stateParams.product;
        return link;
      },
      controller: function($scope,$timeout,$stateParams,$state){
        var setLocalScope = function(urlParam){
          if($stateParams[urlParam]){
            $scope[urlParam] = decodeURIComponent($stateParams[urlParam]); 
          }
        }


        setLocalScope('cat_lvl_1');
        setLocalScope('cat_lvl_2');
        setLocalScope('cat_lvl_3');
        setLocalScope('cat_lvl_4');
        setLocalScope('product');

        $scope.cat_lvl_4_filter = function(item){
          if($scope.cat_lvl_1 != undefined && item.cat_lvl_1 == $scope.cat_lvl_1 && item.cat_lvl_2 == $scope.cat_lvl_2 && item.cat_lvl_3 == $scope.cat_lvl_3
            && item.cat_lvl_4 == $scope.cat_lvl_4 && item.cat_lvl_4 !==undefined){
              return item;       
          }
        }
        $scope.cat_lvl_3_filter = function(item){
          if($scope.cat_lvl_1 != undefined && item.cat_lvl_1 == $scope.cat_lvl_1 && item.cat_lvl_2 == $scope.cat_lvl_2 && item.cat_lvl_3 == $scope.cat_lvl_3 
            && item.cat_lvl_3 !==undefined && $scope.cat_lvl_4 == undefined){
              return item;       
          }
        }
        $scope.cat_lvl_2_filter = function(item){
          if($scope.cat_lvl_1 != undefined && item.cat_lvl_1 == $scope.cat_lvl_1 && item.cat_lvl_2 == $scope.cat_lvl_2 && $scope.cat_lvl_3 == undefined 
            && item.cat_lvl_2 !==undefined){
              return item;       
          }
        }
        $scope.cat_lvl_1_filter = function(item){
          if($scope.cat_lvl_1 != undefined && item.cat_lvl_1 == $scope.cat_lvl_1 && $scope.cat_lvl_2 == undefined){
              return item;       
          }
        }
        $scope.goToAnswerState = function(answersArray,filterBy,filterVal){
          var out = [];
          for (var i = answersArray.length - 1; i >= 0; i--) {
            if(answersArray[i][filterBy] == filterVal){
              out.push(answersArray[i])
            }
          }
          if(out.length == 1){
            console.log(out);
            $state.go('product_a_id',{product:$scope.product,a_id: out[0]['id']});
          }else{
            $state.go('product',{product:$scope.product,cat_lvl_1:$scope.cat_lvl_1,cat_lvl_2:filterVal});
          }
        }
      }
    })
    .state('product_pw_reset', {
      url: "/product_pw_reset?product", 
      templateUrl: function($stateParams){
        var link = "/cc/templates/product?pw_reset="+$stateParams.product+"&product="+$stateParams.product;
        return link;
      },
      controller: function($scope,$timeout,$stateParams){
        
      }
    })
    .state('product_a_id', {
      url: "/product_a_id?product&a_id", 
      templateUrl: function($stateParams){
        var link = "/cc/templates/product?a_id="+$stateParams.a_id+"&product="+$stateParams.product;
        return link;
      },
      controller: function($scope,$timeout,$stateParams){

      }
    })
    .state('product_compat', {
      url: "/product_compat?product", 
      templateUrl: function($stateParams){
        var link = "/cc/templates/product?product_compat="+$stateParams.product+"&product="+$stateParams.product;
        return link;
      },
      controller: function($scope,$timeout,$stateParams){
        
      }
    })
    .state('login_page', {
      url: "/login_page?redirect",   
        templateUrl: "/euf/assets/themes/qsee/partials2/login_page.html",
        controller:function($scope,$stateParams,$timeout,$state){
          if($scope.contact_info != undefined){
            if($stateParams.redirect!=undefined){
              var state = angular.copy($stateParams.redirect);
              $state.go(state);
            }else{
              $state.go('account_overview');
            }
          }
        }        
    })
      .state('rma_home', {
      url: "/rma_home",   
        templateUrl: "/euf/assets/themes/qsee/partials2/rma_home.html",
        controller:function($timeout, $scope, $state,$rootScope){
          $scope.getWherePurchased();
          $scope.getStates();
          $scope.getCountries();   
        }      
    })
    .state('rma_list', {
      url: "/rma_list", 
      templateUrl: "/euf/assets/themes/qsee/partials2/rma_list.html"
      
    })
    .state('rma_form_1', {
      url: "/rma_1", 
      templateUrl: "/euf/assets/themes/qsee/partials2/rma_page_1.html",
      controller:function($scope,$state){
        
      }
    })
    .state('rma_form', {
      url: "/rma_form",
      templateUrl: "/euf/assets/themes/qsee/partials2/rma_form.html",
      controller:function($scope,$state){
        
      }
    })
    .state('rma_form_2', {
      url: "/rma_form_",
      templateUrl: "/euf/assets/themes/qsee/partials2/rma_form.html",
      controller:function($scope,$state){
        
      }
    })
    .state('warranty', {
      url: "/warranty", 
      templateUrl: "/euf/assets/themes/qsee/partials2/warranty.html",
      controller:'search'
      
    })
    .state('account_overview', {
      url: "/account_overview", 
      templateUrl: "/euf/assets/themes/qsee/partials2/account_overview.html",
      controller:function($scope,$state){
        
      }
    })
    .state('registered_products', {
      url: "/registered_products", 
      templateUrl: "/euf/assets/themes/qsee/partials2/registered_products.html"
      
    })
    .state('account_creation', {
      url: "/account_creation", 
      templateUrl: "/euf/assets/themes/qsee/partials2/login_page.html",
      controller:function($scope,$state){
        if($scope.contact_info != undefined){
          $state.go('account_overview');
        }
      }
    })
    .state('incident_show', {
      url: "/incident?i_id", 
      templateUrl:function($stateParams){
        return "/cc/templates/incident?i_id="+$stateParams.i_id
      },
      controller:function($scope,$stateParams){
        if($stateParams.i_id){
          $scope.i_id = $stateParams.i_id;
        }
      }
    })
    .state('incident_show.update', {
      url: "_update", 
      templateUrl:function($stateParams){
        return "/cc/templates/incident?i_id="+$stateParams.i_id
      },
      controller:function($scope,$stateParams){
        if($stateParams.i_id){
          $scope.i_id = $stateParams.i_id;
        }
      }
    })
    .state('incident_show.details', {
      url: "_details", 
      templateUrl:function($stateParams){
        return "/cc/templates/incident?i_id="+$stateParams.i_id
      },
      controller:function($scope,$stateParams){
        if($stateParams.i_id){
          $scope.i_id = $stateParams.i_id;
        }
      }
    })
    .state('incident_show.print', {
      url: "_print", 
      templateUrl:function($stateParams){
        return "/cc/templates/incident?i_id="+$stateParams.i_id
      },
      controller:function($scope,$stateParams){
        if($stateParams.i_id){
          $scope.i_id = $stateParams.i_id;
        }
      }
    })
    .state('incidents_list', {
      url: "/incidents_list", 
      templateUrl: "/euf/assets/themes/qsee/partials2/incidents_list.html",
      controller:function($scope,$state){
        
      }
    })
    .state('incident_confirmation', {
      url: "/incident_confirmation?i_id",   
      templateUrl: function($stateParams){
        return "/cc/templates/incident_confirmation?inc_type=incident&i_id="+$stateParams.i_id
      }
    })
    .state('rma_confirmation', {
      url: "/rma_confirmation?i_id",   
      templateUrl: function($stateParams){
        return "/cc/templates/incident_confirmation?inc_type=rma&i_id="+$stateParams.i_id
      }
    })
    .state('product_registration_confirmation', {
      url: "/product_registration_confirmation?i_id",   
      templateUrl: function($stateParams){
        return "/cc/templates/incident_confirmation?inc_type=prodreg&i_id="+$stateParams.i_id
      }
    })
  })
.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
})
.filter('seriesFilter', function() {
   return function(collection, wordToMatch) {
      var output = [];

      angular.forEach(collection, function(item) {
          if(item['Summary'].indexOf(wordToMatch)!==-1){
          	output.push(item);
          }
      });

      return output;
   };
})
.filter('oldQTSeriesFilter', function() {
   return function(collection) {
      var output = [];

      angular.forEach(collection, function(item) {
          if(item['prod_lvl_2'].indexOf("QT Series")!==-1 && item['Summary'].indexOf("View")==-1 && item['Summary'].indexOf("NEW QT")==-1){
          	output.push(item);
          }
      });

      return output;
   };
})
.filter('firmwareFilter', function() {
   return function(input) {
      if(input.match(/\(([^)]+)\)|(,.*)|(and)|(A)|(-.*)|( \(.*\))/g)){
        input = input.replace(/\(([^)]+)\)|(,.*)|( and.*)|(A|B)|(-.*)|( \(.*\))/g,'');
        return input;
      }else{
        return input
      }
    }
})
.filter('orderObjectBy', function(){
 return function(input, attribute) {
    if (!angular.isObject(input)) return input;

    var array = [];
    for(var objectKey in input) {
        array.push(input[objectKey]);
    }

    array.sort(function(a, b){
        a = parseInt(a[attribute]);
        b = parseInt(b[attribute]);
        return a - b;
    });
    return array;
 }
})
.filter('dateTime', function() {
    return function(input) {
      var date = new Date(input);
      var options = {
          weekday: "long", year: "numeric", month: "short",
          day: "numeric", hour: "2-digit", minute: "2-digit"
      };
      return date.toLocaleTimeString("en-us", options);
    }
})
.filter('startFrom', function() {
    return function(input, start) {
      if(input){
        start = +start; //parse to int
        return input.slice(start);
      }
    }
})

// sets up the order for the answer index ng-repeat
// based on prod_lvl_1
.filter('answerIndexProdOrder', function() {
  return function(items) {
  	var out = [];
    for (var i = items.length - 1; i >= 0; i--) {
    	if(items[i].prod_lvl_1.indexOf('Record') == 0){
    		out.unshift(items[i]);
    	}else{
	    	if(items[i].prod_lvl_1.indexOf('Acc') == 0){
	    		out.push(items[i]);
	    	}else if(items[i].prod_lvl_1.indexOf('Cam') == 0){
	    		out.splice(-1, 0, items[i]);
	    	}
    	} 
    }
  	return out;
  };
})

// sets up the order for the answer index ng-repeat
// based on prod_lvl_2/series
.filter('answerIndexSeriesOrder', function() {
  return function(items) {
  	var out = [];
    for (var i = items.length - 1; i >= 0; i--) {
    	if(items[i].prod_lvl_2.indexOf('QC') == 0 || items[i].prod_lvl_2.indexOf('QT') == 0){
    		out.unshift(items[i]);
    	}else{
    		out.push(items[i]);
    	} 
    }
  	return out;
  };
})
// sets up the order for the answer index ng-repeat
// based on cat_lvl_2
.filter('answerIndexHowToDeviceOrder', function() {
  return function(items) {
  	var out = [];
    for (var i = items.length - 1; i >= 0; i--) {
    	if(items[i].cat_lvl_2.indexOf('Device') == 10){
    		out.unshift(items[i]);
    	}else if(items[i].cat_lvl_2.indexOf('Device') == 17){
    		out.push(items[i]);
    	}else{
    		out.push(items[i]);
    	} 
    }
  	return out;
  };
})
// gets count of answers
// for the how-to's on
// the answer index
.filter('answerIndexCatLvl3Count', function() {
  return function(items,topic){
    var out = [];
    for (var i = items.length - 1; i >= 0; i--) {
      if(items[i].cat_lvl_3 == topic){
        out.push(items[i]);
      }
    }
  return out.length;
  };
})

// reverses the object collection
.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
})

// filter for the keyword search
// let's you search based on any of the object properties
// against a query
// it will split the words too
// so any words that match should match
.filter('keywordSearch',function(){
  return function(answers, props, query){
    if(query!=undefined){
      var regexMatch = new RegExp("(?=.*" + query.toLowerCase().split(/\s+/).join(")(?=.*") + ").*",'g');
      var out = [];
      for (var i = 0; i < answers.length; i++){
        for (var j = props.length - 1; j >= 0; j--) {
          if(answers[i][props[j]]!== undefined){
            if(typeof props[j] == "string"){
              if(answers[i][props[j]].toLowerCase().match(regexMatch)){
                out.push(answers[i]);
              }
            }else if(typeof props[j] == "number"){
              if(answers[i][props[j]].match(regexMatch)){
                out.push(answers[i]);
              }
            }
          }
        }
      };
    }

      return out;
    }
})

// filter is for the product page
// I was originally using ng-if to dynamically load answers
// but wanted to use a filter so that I could
// get answer counts for a given category on a product page

.filter('howToAnswers', function() {
  return function(items,level1,level,param1,param2,param3,param4) {
    var out = [];
    for(var i = 0; i < items.length; i++){
      if(items[i].cat_lvl_1!== undefined && items[i].cat_lvl_1== level1){
        if(level == 2 && 
        items[i].cat_lvl_2!== undefined && 
        param1 == undefined &&
        param2 == undefined &&
        param3 == undefined &&
        param4 == undefined){
          out.push(items[i]);
        }
        if(level == 3 && 
        items[i].cat_lvl_2!== undefined &&
        items[i].cat_lvl_2 == param2 &&
        param3 == undefined &&
        param4 == undefined){
          out.push(items[i]);
        }
        if(level == 4 &&
        items[i].cat_lvl_2!==undefined &&
        items[i].cat_lvl_2 == param2 &&
        items[i].cat_lvl_3!==undefined &&
        items[i].cat_lvl_3 == param3 &&
        param4 == undefined
        ){
          out.push(items[i]);
        } 
      } 
    };
  return out;
  };
})
// helps get the length of the answer counts for a given category
.filter('howToAnswersLength', function() {
  return function(items,cat_lvl_2,cat_lvl_3,cat_lvl_4) {
  var out = [];
    for(var i = 0; i < items.length; i++){
      if(items[i].cat_lvl_2!== undefined && items[i].cat_lvl_2 == cat_lvl_2){
        out.push(items[i]);
      }else if(items[i].cat_lvl_3!== undefined && items[i].cat_lvl_3 == cat_lvl_3){
        out.push(items[i]);
      }else if(items[i].cat_lvl_4!== undefined && items[i].cat_lvl_4 == cat_lvl_4){
        out.push(items[i]);
      }
    };
    return out.length;
  };
})
.filter('productAnswerFilter', function() {
  return function(items, cat_lvl_1, cat_lvl_2, notFirmware) {
    var out = [];
    var localCatLvl2 = cat_lvl_2;
    
    function testIfCatLvl2(answer_cat_lvl_2){
      if(localCatLvl2!==undefined){
        if(answer_cat_lvl_2 === localCatLvl2){
          return true;
        }else{
          return false;
        }
      }else{
        return true;
      }
    }

    for(var i = 0; i < items.length; i++){
      if(notFirmware ==false && items[i].cat_lvl_1!== undefined && items[i].cat_lvl_1!=='How-tos' &&
      cat_lvl_1 == undefined && cat_lvl_2 == undefined){
        out.push(items[i]);
      }else if(notFirmware ==true && items[i].cat_lvl_1!== undefined && items[i].cat_lvl_1!=='How-tos' &&
      items[i].cat_lvl_1!=='Firmware' && cat_lvl_1!==undefined && items[i].cat_lvl_1 == cat_lvl_1 && testIfCatLvl2(items[i].cat_lvl_2)){
        out.push(items[i]);
      }
    };
    return out;
  };
})
.filter('filteredProductAnswerLength', function() {
  return function(items, cat_lvl_1,cat_lvl_2) {
    var out = [];
    var localCatLvl2 = cat_lvl_2;
    
    function testIfCatLvl2(answer_cat_lvl_2){
      if(localCatLvl2!==undefined){
        if(answer_cat_lvl_2 === localCatLvl2){
          return true;
        }else{
          return false;
        }
      }else{
        return true;
      }
    }

    for(var i = 0; i < items.length; i++){
      if(items[i].cat_lvl_1!== undefined && items[i].cat_lvl_1==cat_lvl_1 && items[i].cat_lvl_1!=='How-tos' && testIfCatLvl2(items[i].cat_lvl_2)){
        out.push(items[i]);
      }
    };
    return out.length;
  };
})
.directive('checkBeforeSubmitting', function($timeout){
  return{
    restrict: 'A',
    link: function(scope,element,attrs){
      $timeout(function(){
        var anchorTarget = document.querySelectorAll('[hide-me-when-unchecked]')[0];
        anchorTarget.style.display = 'none';
        element[0].addEventListener('click',function(){
          anchorTarget.style.display = anchorTarget.style.display == 'none' ? 'block' : 'none'; 
        })
      })
    }
  }
})
.directive('toggleChild',function($timeout){
  return{
    restrict:'A',
    link: function(scope,element,attrs){
      $timeout(function(){
        var target = element[0].nextElementSibling;
        var targetHeight;
        if(target){
          targetHeight = target.offsetHeight;
          $timeout(function(){
            target.style.display ='none';
            // target.style.height = 0;
            target.style.opacity = 0;
            target.style.visibility = 'hidden';
          });
          element[0].addEventListener('click',function(){
            if(target.style.display =='block'){
              $timeout(function(){
                var opacity = 1;
                var myInterval = setInterval(function(){
                    if(opacity > 0 && opacity <= 1){
                      opacity = opacity - .05;
                      target.style.opacity = opacity;
                      if(targetHeight > 0){
                        target.style.height = ((targetHeight*opacity)+20)+'px';
                      }
                    }else{
                      target.style.display ='none';
                      if(targetHeight > 0){
                        target.style.height = targetHeight;
                      }
                      target.style.visibility = 'hidden';
                      clearInterval(myInterval);
                    }
                },20);
                $timeout(function(){
                  return myInterval; 
                },50);
              });
            }else{
              target.style.visibility = 'inherit';
              target.style.display ='block';
              var opacity = 0;
              var myInterval = setInterval(function(){
                  if(opacity >=0 && opacity < 1){
                    opacity = opacity + .05;
                    target.style.opacity = opacity;
                    if(targetHeight > 0){
                      target.style.height = ((targetHeight*opacity)+20)+'px';
                    }
                  }else{
                    if(attrs.scroll == 'true'){
                      smoothScroll(element[0].id)
                    }
                    if(targetHeight > 0){
                      target.style.height = targetHeight;
                    }
                    clearInterval(myInterval);
                  }
              },20);
              $timeout(function(){
                return myInterval; 
              },50);
            }
          })
        }
      })
    }
  }
})
.directive('waitToLoad',function($timeout){
  return{
    restrict:'A',
    link: function(scope,elem,attrs){
      $timeout(function(){
        elem[0].className = elem[0].className.replace('hidden','');
      },700)  
    }
  }
})
.directive('prodRegSync',function($timeout){
  return{
    restrict:'A',
    link: function(scope,elem,attrs){
      $timeout(function(){
        if(scope.detectState('rma_form_2')==true || scope.detectState('ask_a_question')==true){
          elem[0].addEventListener('click',function(){
            scope.productRegistrationSync(attrs.prodRegSync);
          })
        }
      })  
    }
  }
})
.directive('searchInputFocusCheck',function($timeout){  
  return{
    restrict:'A',
    link: function(scope,elem,attrs){
      $timeout(function(){
        elem[0].addEventListener('focus',function(){
          scope.searchInputFocused = true;
        })
        elem[0].addEventListener('blur',function(){
          scope.searchInputFocused = false;
        })
      })  
    }
  }
})
.directive('productSelectWidget',function($parse,$timeout){
  return{
    restrict:'E',
    require: ['^form'],
    controller:'search',
    template: '<div class="product-select uncentered" ng-class="{&#39; has-error&#39;: formProductInvalid && (formProductTouched || submitted==true) }">'+
                '<div class="product-select-widget ">'+
                  '<div ng-hide="currentProduct!=undefined" class=" uncentered">'+
                    '<div class="login" toggle-child>'+
                      '<span class="drop-down-arrow text-center pull-right"><i class="fa fa-sort-desc"></i></span>'+
                    '</div>'+  
                    '<div wait-to-load class="hidden toggled-products">'+
                      '<div wait-to-load class="container hidden" ng-if="!detectState(&#39;product_registration_&#39;) && contact_info !=undefined && contact_info.prod_reg_incidents.length > 0">'+
                        '<a ng-click="selectThisProd()" class="pointer white-text fading-In" toggle-child>'+
                          '- My Registered Products</a>'+
                        '<div style="margin-left:30px;">'+
                          '<div style="padding: 5px 0px" ng-repeat="product in ::contact_info.prod_reg_incidents">'+
                            '<a class="pointer white-text fading-In" prod-reg-sync="{{product}}" ng-click="setProduct(&#39;show&#39;,product.product_name,product.product_id)">- {{product.product_name}}</a>'+
                          '</div>'+
                        '</div> '+
                      '</div>'+
                      '<div wait-to-load class="container hidden" ng-repeat="(key, value) in products">'+
                        '<a ng-click="selectThisProd()" class="pointer white-text fading-In" toggle-child>'+
                          '- {{::key}}</a>'+
                        '<div style="margin-left:30px;">'+
                          '<div style="padding: 5px 0px" ng-repeat="product in ::value | unique:&#39;series&#39;">'+
                            '<a class="pointer white-text fading-In" ng-click="selectedSeries = selectedSeries == product.series ? &#39;none&#39; : product.series" toggle-child >- {{::product.series}}</a>'+
                            '<div style="margin-left:30px; padding: 5px 0px" ng-repeat="product in ::value | orderBy:&#39;name&#39;" ng-if="product.series==selectedSeries">'+
                              '<a class="pointer white-text fading-In" prod-reg-sync="clear" ng-click="setProduct(&#39;show&#39;,product.name,product.id)">- {{::product.name}}</a>'+
                            '</div>'+
                          '</div>'+
                        '</div> '+
                      '</div>'+
                      '<a class="give-me-space white-text" ui-sref="answer({a_id: 1175})">Where is my model #?</a>'+
                    '</div>'+
                  '</div>'+
                  '<div class="text-center" ng-hide="currentProduct==undefined" >'+
                   '<a ng-click="setProduct(&#39;hide&#39;)">'+
                     '<img class="fading-In img-responsive" ng-src="//q-see.s3.amazonaws.com/content/support/prodImage/{{currentProduct}}.jpg">'+
                   '<br><input ng-model="currentProduct" name="product" type="hidden" required>{{currentProduct}}</input></a>'+
                  '</div>'+
                '</div>'+
                '<div class="container-fluid has-error">'+
                  '<p class="text-center help-block fading-In"  ng-show="formProductInvalid && (formProductTouched || submitted==true)">The product field is required</p>'+
                '</div>'+
              '</div>',
    link: function (scope, elem, attrs, ctrls) {
      scope.formProductTouched = false;
      scope.formProductInvalid = true;
      elem[0].addEventListener('click',function(){
        var model = $parse(attrs.form);
        var modelObject = model(scope);
        modelObject.product.$pristine = false;
        modelObject.product.$untouched = false;
        modelObject.product.$touched = true;
        var recursiveCall = function(){
          $timeout(function(){
            if(modelObject.product){
              scope.formProductTouched = modelObject.product.$touched;
              scope.formProductInvalid = modelObject.product.$invalid;
              if(modelObject.product.$modelValue==undefined){
                scope.rmaProofOfPurchase = true;
              }
            }else{
              recursiveCall();
            }
          },100);
        }
        recursiveCall();
      })
    }
  }
})  
.directive('countrySelectWidget',function($parse){
  return{
    restrict:'E',
    replace:true,
    template: '<div ng-if="contact_method!=&#39;chat&#39;" class="" ng-class="{&#39;has-error&#39;: {{form}}.country_id.$invalid && (!{{form}}.country_id.$pristine || submitted==true)}">'+
                '<div class="">'+
                  '<span class="drop-down-arrow text-center pull-right country-select-arrow"><i class="fa fa-sort-desc"></i></span>'+
                  '<select name="country_id" class="country-select" ng-model="country_id" required="true" ng-init="setCountry(contact_info.country_id)" ng-blur="setCountry(country_id)" >'+
                    '<option value="">Please select country where you are located</option>'+
                    '<option ng-repeat="country in countries" ng-selected="country_id == country.id"  value="{{country.id}}" >{{country.name}}</option>'+
                  '</select>'+
                  '<p ng-show="{{form}}.country_id.$invalid && (!{{form}}.country_id.$pristine || submitted==true)" class="help-block fading-In">The country field is required</p>'+
                '</div>'+
              '</div>',
    link: function (scope, element, attrs) {
            scope.form = attrs.form;
        }
  }
})
.directive('stateSelectWidget',function($parse){
  return{
    restrict:'E',
    replace:true,
    template: '<div ng-if="contact_method!=&#39;chat&#39;" class="" ng-class="{&#39;has-error&#39;: {{form}}.state_id.$invalid && (!{{form}}.state_id.$pristine || submitted==true)}">'+
                '<div class="">'+
                  '<span class="drop-down-arrow text-center pull-right country-select-arrow"><i class="fa fa-sort-desc"></i></span>'+
                  '<select name="state_id" class="country-select" ng-model="state_id" required="true" ng-init="setState(contact_info.prov_id)" ng-blur="setState(state_id)" >'+
                    '<option value="">Please select the state/province where you are located</option>'+
                    '<option ng-hide="country_id!=1" ng-repeat="state in states | orderBy:&#39;name&#39;" ng-selected="state_id == state.id"  value="{{state.id}}" >{{state.name}}</option>'+
                    '<option ng-show="country_id!=1" value="291" >If necessary, please specify this in the city field above</option>'+
                  '</select>'+
                  '<p ng-show="{{form}}.state_id.$invalid && (!{{form}}.state_id.$pristine || submitted==true)" class="help-block fading-In">The state/province field is required</p>'+
                '</div>'+
              '</div>',
    link: function (scope, element, attrs) {
            scope.form = attrs.form;
        }
  }
})
.directive('loginForm',function(){
  return{
    restrict:'E',
    replace:true,
    template:'<div class="account-login card">'+
                '<br>'+
                '<div class="container-fluid card-header text-uppercase text-center">'+
                  'Log in form'+
                '</div>'+
                '<form class="card-body text-center" name="liForm" ng-keyup="$event.keyCode == 13 && logInHandler(liForm.$valid,&#39;in&#39;,login,password,redirect)" novalidate>'+
                  '<br>'+
                  '<div class="container-fluid has-error"><p class="help-block" ng-class="{&#39; fading-In &#39;:loginError!=undefined}">{{loginError == undefined ? "You must log in before viewing this page" : loginError}}</p></div>'+
                  '<br>'+
                  '<div class="container-fluid form-field"  ng-class="{&#39; has-error&#39;: liForm.login.$invalid && (!liForm.login.$pristine || submittedLogin==true)}">'+
                    '<label class="form-label uncentered" >Login:</label>'+
                    '<input name="login" ng-model="login" class="login" required="true">'+
                    '<p ng-show="liForm.login.$invalid && (!liForm.login.$pristine || submittedLogin==true)" class="help-block fading-In">Your login is required.</p>'+
                  '</div>'+
                  '<div class="container-fluid form-field"  ng-class="{&#39; has-error&#39;: liForm.password.$invalid && (!liForm.password.$pristine || submittedLogin==true)}">'+
                    '<label class="form-label uncentered" >Password:</label>'+
                    '<input name="password" ng-model="password" class="login" type="password" required="true">'+
                    '<p ng-show="liForm.password.$invalid && (!liForm.password.$pristine || submittedLogin==true)" class="help-block fading-In">Your password is required.</p>'+
                  '</div>'+
                  '<br>'+
                  '<div class="text-uppercase"><submit-button button-label="LOG IN" ng-click="logInHandler(liForm.$valid,&#39;in&#39;,login,password,redirect)"></submit-button></div>'+
                  '<br>'+
                  '<a class="fp-link text-underline" ui-sref="account_assistance" >Forgot your username or password?</a>'+
                  '<br><br>'+
                  '<a class="fp-link text-underline" ui-sref="account_creation" >Click here to create an account</a>'+
                  '<br><br>'+
                '</form>'+
              '</div>'
  }
})
.directive('submitButton',function(){
  return{
    restrict:'E',
    replace:true,
    template: '<div class="text-center">'+
                '<a class="large-text large-button pointer text-uppercase" disable-on-submit>{{buttonLabel}}</a>'+
                '<div class="hidden-spinner hidden fading-In">'+
                  '<span class="lead">Submitting...</span>'+
                  '<i class="fa fa-2x fa-spinner fa-spin"></i>'+
                '</div>'+
              '</div>',
    link: function (scope, element, attrs) {
            scope.buttonLabel = attrs.buttonLabel;
        }
  }
})
.directive('accountInfoWidget',function(){
  return{
    restrict:'E',
    template:'<form name="accountInfoForm" class="form card-body text-center col-lg-12 col-md-12 col-sm-12 col-xs-12 account-info-form less-margin extra-bottom" novalidate>'+
                '<br class="hidden-sm hidden-xs"><br>'+
                '<p class="text-center " style="margin:0 1px">'+
                  '<span class="lead"> Please confirm your contact information:</span>'+
                '</p>'+
                '<br>'+
                '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 first half form-field" ng-class="{&#39; has-error&#39;: accountInfoForm.first_name.$invalid && (!accountInfoForm.first_name.$pristine || submitted==true)}">'+
                  '<label class="form-label uncentered" >First Name:</label>'+
                  '<input name="first_name" ng-model="contact_info.first_name"  class="login" required="true">'+
                  '<p ng-show="accountInfoForm.first_name.$invalid && (!accountInfoForm.first_name.$pristine || submitted==true)" class="help-block fading-In text-center">Your first name is required.</p>'+
                '</div>'+
                '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 half form-field"  ng-class="{&#39; has-error&#39;: accountInfoForm.last_name.$invalid && (!accountInfoForm.last_name.$pristine || submitted==true)}">'+
                  '<label class="form-label uncentered" >Last Name:</label>'+
                  '<input name="last_name" ng-model="contact_info.last_name"  class="login"  required="true">'+
                  '<p ng-show="accountInfoForm.last_name.$invalid && (!accountInfoForm.last_name.$pristine || submitted==true)" class="help-block fading-In text-center">Your last name is required.</p>'+
                '</div>'+
                '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 first half form-field"  ng-class="{&#39; has-error&#39;: accountInfoForm.email.$invalid && (!accountInfoForm.email.$pristine || submitted==true)}">'+
                  '<label class="form-label uncentered" >Email Address:</label>'+
                  '<input type="email" ng-model="contact_info.email" name="email"  class="login" required="true">'+
                  '<p ng-show="accountInfoForm.email.$invalid && (!accountInfoForm.email.$pristine || submitted==true)" class="help-block fading-In text-center">Your email is required; it is currently empty or in the wrong format.</p>'+
                '</div>'+
                '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 half form-field"  ng-class="{&#39; has-error&#39;: accountInfoForm.primary_phone.$invalid && (!accountInfoForm.primary_phone.$pristine || submitted==true)}">'+
                  '<label class="form-label uncentered" >Primary Phone Number:</label>'+
                  '<input ng-model="contact_info.ph_home" name="primary_phone"  class="login" required="true">'+
                  '<p ng-show="accountInfoForm.primary_phone.$invalid && (!accountInfoForm.primary_phone.$pristine || submitted==true)" class="help-block fading-In text-center">Your phone number is required.</p>'+
                '</div>'+
                '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 first half form-field"  ng-class="{&#39; has-error&#39;: accountInfoForm.street.$invalid && (!accountInfoForm.street.$pristine || submitted==true)}">'+
                  '<label class="form-label uncentered" >Street Address:</label>'+
                  '<input ng-model="contact_info.street" name="street"  class="login" required="true">'+
                  '<p ng-show="accountInfoForm.street.$invalid && (!accountInfoForm.street.$pristine || submitted==true)" class="help-block fading-In text-center">Your street address is required.</p>'+
                '</div>'+
                '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 half form-field"  ng-class="{&#39; has-error&#39;: accountInfoForm.city.$invalid && (!accountInfoForm.city.$pristine || submitted==true)}">'+
                  '<label class="form-label uncentered" >City:</label>'+
                  '<input ng-model="contact_info.city" name="city"  class="login" required="true">'+
                  '<p ng-show="accountInfoForm.city.$invalid && (!accountInfoForm.city.$pristine || submitted==true)" class="help-block fading-In text-center">Your city is required.</p>'+
                '</div>'+
                '<div class="container-fluid form-field country-field">'+
                  '<label class="form-label uncentered" >Country:</label>'+
                  '<country-select-widget form="accountInfoForm"></country-select-widget>'+
                '</div>'+
                '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 first half form-field">'+
                  '<label class="form-label uncentered" >State:</label>'+
                  '<state-select-widget form="accountInfoForm"></state-select-widget>'+
                '</div>'+
                '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 half form-field"  ng-class="{&#39; has-error&#39;: accountInfoForm.postal_code.$invalid && (!accountInfoForm.postal_code.$pristine || submitted==true)}">'+
                  '<label class="form-label uncentered" >Postal Code:</label>'+
                  '<input ng-model="contact_info.postal_code" name="postal_code"  class="login" required="true">'+
                  '<p ng-show="accountInfoForm.postal_code.$invalid && (!accountInfoForm.postal_code.$pristine || submitted==true)" class="help-block fading-In text-center">Your postal code is required.</p>'+
                '</div>'+
                '<br class="hidden-sm hidden-xs"><br class="hidden-sm hidden-xs"><br class="hidden-sm hidden-xs"><br class="hidden-sm hidden-xs">'+
                '<div ng-if="accountSubmit(&#39;rma_form&#39;)" class="clearfix text-center">'+
                  '<submit-button button-label="Everything is good" ng-click="contactInfoUpdate(accountInfoForm,currentState,&#39;rma_form_2&#39;)"></submit-button>'+
                '</div>'+
                '<div ng-if="accountSubmit(&#39;product_registration&#39;)" class="clearfix text-center">'+
                  '<submit-button button-label="Confirm" ng-click="contactInfoUpdate(accountInfoForm,currentState,&#39;product_registration_&#39;)"></submit-button>'+
                '</div>'+
                '<div ng-if="accountSubmit(&#39;account_settings&#39;)" class="clearfix text-center">'+
                  '<submit-button button-label="Update Account" ng-click="contactInfoUpdate(accountInfoForm,currentState)"></submit-button>'+
                '</div>'+
                '<br>'+
                '<p ng-if="contactUpdateError" class="lead fading-In text-center">{{contactUpdateError}}</p>'+
                '<br class="hidden-sm hidden-xs"><br>'+
              '</form>'
  }
})
.directive('scrollSpy',function($window){
  return{
    restrict:'A',
    link:function(scope, ele, attrs){
      // borrowed from http://stackoverflow.com/a/24829409
      function getPosition(element) {
          var xPosition = 0;
          var yPosition = 0;

          while(element) {
              xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
              yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
              element = element.offsetParent;
          }

          return { x: xPosition, y: yPosition };
      }

      var distanceFromTop = getPosition(ele[0])['y'];
      var distanceFromLeft = getPosition(ele[0])['x'];
      var elementWidth = ele[0].offsetWidth;

      $window.addEventListener('scroll',function(){
        if($window.innerWidth > 767 && $window.pageYOffset >(distanceFromTop+250)){
          ele[0].style.position = "fixed";
          ele[0].style.top = (distanceFromTop / 4)+'px';
          ele[0].style.margin = '0';
          ele[0].style.width = elementWidth+'px';
        }else if($window.pageYOffset < (distanceFromTop+120)){
          ele[0].style.position = "relative";
          ele[0].style.top = '0px';
        }
      })
    }
  }
})
.directive('dynamic', function ($compile,$timeout) {
  return {
    restrict: 'A',
    replace: true,
    transclude: true,
    link: function (scope, ele, attrs) {
      //this is so we can have content load in and bind to the DOM via AJAX
      scope.$watch(attrs.dynamic, function(html) {
        var answerContainer = document.getElementById('answer-container');
        if(answerContainer){
          answerContainer.parentElement.removeChild(answerContainer);
        }
        $timeout(function(){
          ele.html(html);
          $compile(ele.contents())(scope)
        })  
      });
    }
  };
})
.directive('checkDates', function ($timeout) {
  return {
    restrict: 'A',
    link: function (scope, ele, attrs) {
      $timeout(function(){
        // This directive is supposed to work with the date-time-selector widget
        // this is so we can do form validations while using the awesome widget
        // we are testing to make sure that the selected date meets the following criteria:
        //// selected dates can never be older than 2013
        //// selected dates can never be set past today's date (no future dates)
        //// selected dates are in a valid format
        //// for the RMA form
        //////// we need to a check if the date is older than 6 months
        //////////// if it is
        //////////////// tell the customers 'hey, we can only promise a like new product or better'
        //////////// if it ain't
        //////////////// tell the customers 'hey, you are gonna get something brand new AND we will send you an advanced replacement...'
        //// end

        // this function is so we can get the Form name without hardcoding it in
        // we are basically searching for the form
        // by investigating the properties of the current scope
        // and returning the ones that have 'Form' in the property, there should only be one form...
        
        // always name your forms with 'Form' in it!
        // otherwise this won't work if you need to use the date-time-selector widget in a form

        var getKeys = function(obj){
           for(var key in obj){
              if(key.match('Form')){
                return key;
              }
           }
        }

        var dateButtons = document.querySelectorAll('._720kb-datepicker-calendar-day');

        // when the input for the date time is blurred
        // we kind of want to do some checks against the date_purchased value
        // This is because we cannot trust user input

        function doTheFollowing(){
          
          // this is an optional portion for the RMA form
          // this is to instruct customers of their RMA options
          function checkDateToManageCustomerExpectations(dateTime){ 

            // This attrs check is to differentiate the Product Registration form
            // from the RMA form, but can be used to check other forms
            // that may need this widget

            if(attrs.prodReg!= undefined && attrs.prodReg== 'false'){
              var defaultMessage = " All customers who submit defective products to Q-See more than 6 months from the original date of purchase will receive either of the following: (i) a factory reconditioned version of the same model or (ii) a factory reconditioned version of a comparable similar model.";
              var within6 = "You have indicated you purchased a Q-See product within the last 6 months. After reviewing your proof of purchase and the additional criteria listed above, we will replace your product with a brand new model. If your exact model is unavailable, you will receive a comparable brand new model of equal or greater value.\n\nBe advised, customers who input an incorrect original purchase date when submitting their RMA will be subject to different warranty terms and conditions.";

              // I cannot believe
              // that this is all it takes
              // to decide whether or not a customer has purchased their product within the last 6 months
              // we have given them some wiggle room
              // because we are not machines

              var sixMonthsAgo = new Date();
              sixMonthsAgo.setMonth(sixMonthsAgo.getMonth() - 6);
              sixMonthsAgo.setDate(sixMonthsAgo.getDate() - 5);
              scope.within_6_months = (sixMonthsAgo <= dateTime)? within6 : defaultMessage;
            }
          }

          // check the date that has been selected

          function isValidDate(d) {
            var testDate = new Date(d);
            // set the date_purchased field of the form that we are in to either invalid or valid depending on the value entered in
            if(getKeys(scope)!=null){
              if(testDate == 'Invalid Date'){
                scope[getKeys(scope)]['date_purchased'].$invalid = true ;
              }else{
                scope[getKeys(scope)]['date_purchased'].$invalid = false;
                checkDateToManageCustomerExpectations(testDate);
              }
            }else{
              console.log('You forgot to name you form exactly like how you have named all of the other forms on this site..... for shame!');
            }
          }
          
          // give it a second and then check if the date that was entered in is actually valid
          $timeout(function(){
            isValidDate(scope.date_purchased);
          },100);


          // let's compare the dates and make sure people are NOT setting dates past today's date
          if(scope.compareDates(scope.today(),scope.date_purchased)==true){

            // if we compare the dates and the inputted date is larger than today's date
            // we set the date_purchased field as invalid
            scope[getKeys(scope)]['date_purchased'].$invalid = true;
            scope[getKeys(scope)]['date_purchased'].$valid = false;
          }else{
            // otherwise, if everything is right in the world, we can set things right
            scope[getKeys(scope)]['date_purchased'].$valid = true;
            scope[getKeys(scope)]['date_purchased'].$invalid = false;
          }

          // let's compare the dates and make sure people are setting dates past 2012
          if(scope.compareDates(scope.date_purchased, new Date('1/1/2013'))==true){
            scope[getKeys(scope)]['date_purchased'].$invalid = true;
            scope[getKeys(scope)]['date_purchased'].$valid = false;
            scope.youHaveAnOldProduct = true;
          }else{
            scope[getKeys(scope)]['date_purchased'].$invalid = true;
            scope[getKeys(scope)]['date_purchased'].$valid = false;
            scope.youHaveAnOldProduct = false;
          }
        }

        // I wrote the little piece below
        // because watching the scope kept creating 
        // a ton of watchers
        // so I used DOM events instead 
        // test

        //make an array of applicable events that will update the scope
        var events = ['click','focus','blur','keydown'];

        //loop through each type of event
        for(var i = events.length; i > 0; i--){
          
          //bind the element to the event
          ele[0].addEventListener(events[i],function(){
            
                      //do various scope checks and validations based on the above
            doTheFollowing();

            //once the scope has been manually updated, test to see if it is valid
            var preparedDate = new Date(scope.date_purchased);

            //if it is valid
            if(preparedDate!="Invalid Date"){

              //set a date value that the server can recognize
              scope.date_purchased_ISO = preparedDate.toISOString();
            }
          }) 
        }
      })
    }
  };
})
.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}]);