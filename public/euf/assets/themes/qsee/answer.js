app.controller('answer', function($timeout){
	var self = this;
    self.language;
    
    if(document.domain == "qsee-jp.custhelp.com"){
        self.language = 'jp';
    }else{
        self.language = 'en';
    }
    self.changeLanguage = function(lan){
        self.language = lan;
    }
})
.directive('removeUpperElems', function($timeout){
    return {
        controller: 'answer',
        link: function(scope, elem, attrs){
            var self = this;
            $timeout(function(){
                var pad30 = document.querySelectorAll('.pad30');
                // var title = document.getElementById('rn_PageTitle');
                if(pad30.length){
                    pad30[0].classList.remove('pad30');
                }
                // if(title){
                //     title.style.display = 'none';
                // }
                if(document.getElementById('rn_MainColumn')){
                    document.getElementById('rn_MainColumn').className = 'container-fluid';
                }
            },500)
        }
    }
});